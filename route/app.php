<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: sueRimn
 * @Date: 2020-05-05 19:26:53
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-05-05 19:47:08
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;


//获取中国城市信息
Route::get('/city', '\app\admin\controller\system\china@index');



//客户信息的一些信息获取
Route::group('/customer', function () {
  //客户等级
  Route::get('/level', '\app\admin\controller\crm\customerlevel@index');
  //客户行业
  Route::get('/trade', '\app\admin\controller\crm\customertrade@index');
  //客户源
  Route::get('/origin', '\app\admin\controller\crm\customerorigin@index');
});
