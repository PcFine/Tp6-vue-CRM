/*
 Navicat MySQL Data Transfer

 Source Server         : 腾讯高可用版
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : 59507f211162e.gz.cdb.myqcloud.com:17014
 Source Schema         : 11111

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 06/04/2020 16:11:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yl_admin
-- ----------------------------
DROP TABLE IF EXISTS `yl_admin`;
CREATE TABLE `yl_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_admin
-- ----------------------------
INSERT INTO `yl_admin` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 1586052521, '220.200.34.151', 1081, 'd', 1555249039, 1586052521);

-- ----------------------------
-- Table structure for yl_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `yl_admin_log`;
CREATE TABLE `yl_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `yl_auth_group`;
CREATE TABLE `yl_auth_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_auth_group
-- ----------------------------
INSERT INTO `yl_auth_group` VALUES (1, '超级管理员', '', 1, '6,43,44,110,111,120,121,122,112,117,118,119,113,114,115,116,125,126,127,128,129,130,131,132,4,123,92,124,45,68,69,70,103,5,16,37,38,39,18,53,17,40,41,42,15,22,23,24');

-- ----------------------------
-- Table structure for yl_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `yl_auth_group_access`;
CREATE TABLE `yl_auth_group_access`  (
  `uid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_auth_group_access
-- ----------------------------
INSERT INTO `yl_auth_group_access` VALUES (1, 1);
INSERT INTO `yl_auth_group_access` VALUES (2, 1);

-- ----------------------------
-- Table structure for yl_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yl_auth_rule`;
CREATE TABLE `yl_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_auth_rule
-- ----------------------------
INSERT INTO `yl_auth_rule` VALUES (4, 0, '系统设置', '', 'layui-icon-set', 5, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (5, 0, '管理员管理', '', 'layui-icon-auz', 6, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (6, 0, '控制台', 'admin/index/home', 'layui-icon-home', 1, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (15, 5, '权限规则', 'admin/auth/rule', 'fa fa-th-list', 3, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (16, 5, '管理员列表', 'admin/admin/index', 'fa fa-user', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (17, 5, '角色管理', 'admin/auth/group', 'fa fa-users', 2, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (18, 5, '操作日志', 'admin/admin/log', 'fa fa-clock-o', 1, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (22, 15, '添加', 'admin/auth/addRule', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (23, 15, '编辑', 'admin/auth/editRule', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (24, 15, '删除', 'admin/auth/delRule', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (37, 16, '添加', 'admin/admin/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (38, 16, '编辑', 'admin/admin/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (39, 16, '删除', 'admin/admin/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (40, 17, '添加', 'admin/auth/addGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (41, 17, '编辑', 'admin/auth/editGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (42, 17, '删除', 'admin/auth/delGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (43, 6, '修改密码', 'admin/index/editPassword', '', 2, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (44, 6, '清除缓存', 'admin/index/clear', '', 3, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (45, 4, '上传设置', 'admin/config/upload', 'fa fa-upload', 4, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (53, 18, '一键清空', 'admin/admin/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (68, 45, '上传图片', 'admin/index/uploadimage', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (69, 45, '上传文件', 'admin/index/uploadfile', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (70, 45, '上传视频', 'admin/index/uploadvideo', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (92, 4, '配置信息', 'admin/config/param', '', 2, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (103, 4, '上传管理', 'admin/uploads/index', '', 10, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (110, 0, '商户管理', 'admin/shop/index', 'layui-icon-user', 2, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (111, 110, '商户列表', 'admin/shop/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (112, 110, '套餐管理', 'admin/shop/taocan', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (113, 110, '权限列表', 'admin/user_auth/rule', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (114, 113, '添加', 'admin/user_auth/addGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (115, 113, '编辑', 'admin/user_auth/editGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (116, 113, '删除', 'admin/user_auth/delGroup', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (117, 112, '添加', 'admin/shop/add_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (118, 112, '编辑', 'admin/shop/edit_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (119, 112, '删除', 'admin/shop/del_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (120, 111, '新增', 'admin/shop/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (121, 111, '编辑', 'admin/shop/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (122, 111, '删除', 'admin/shop/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (123, 4, '系统信息', 'admin/config/setting', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (124, 4, '系统设置', 'admin/config/system', '', 3, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (125, 110, '字典管理', 'admin/dict/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (126, 125, '新增', 'admin/dict/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (127, 125, '编辑', 'admin/dict/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (128, 125, '删除', 'admin/dict/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (129, 110, '自定义字段', 'admin/field/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (130, 129, '新增', 'admin/field/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (131, 129, '编辑', 'admin/field/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_auth_rule` VALUES (132, 129, '删除', 'admin/field/del', '', 0, 'auth', 0, 1);

-- ----------------------------
-- Table structure for yl_crm_contact
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_contact`;
CREATE TABLE `yl_crm_contact`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(5) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `type_id` mediumint(10) NOT NULL DEFAULT 0 COMMENT '联系人类型',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `wechat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信',
  `status` int(2) NULL DEFAULT NULL COMMENT '状态 0禁用 1启用',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户联系人表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_crm_contract
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_contract`;
CREATE TABLE `yl_crm_contract`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(5) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `photo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '多图文件',
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同备注',
  `start_time` int(10) NOT NULL DEFAULT 0 COMMENT '合同开始时间',
  `end_time` int(10) NOT NULL DEFAULT 0 COMMENT '合同结束时间',
  `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '合同状态 0待审核 1已审核',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_crm_customer
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_customer`;
CREATE TABLE `yl_crm_customer`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(3) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名称',
  `sname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户简称',
  `industry` mediumint(10) NOT NULL DEFAULT 0 COMMENT '行业',
  `source` mediumint(10) NOT NULL DEFAULT 0 COMMENT '来源',
  `level` mediumint(10) NOT NULL DEFAULT 0 COMMENT '级别',
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `area` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `coo_lat` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标LAT',
  `coo_lng` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标lng',
  `status` mediumint(10) NOT NULL DEFAULT 0 COMMENT '状态',
  `next_time` int(10) NOT NULL DEFAULT 0 COMMENT '下次联系时间',
  `jdata` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自定义字段内容',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '创建ID',
  `sort_order` tinyint(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注信息',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户表\r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_crm_sales
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_sales`;
CREATE TABLE `yl_crm_sales`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `user_id` int(5) NULL DEFAULT NULL COMMENT '操作用户',
  `customer_id` int(5) NOT NULL COMMENT '客户ID',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '自定义字段',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_crm_track
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_track`;
CREATE TABLE `yl_crm_track`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(5) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '自定义字段',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_crm_user
-- ----------------------------
DROP TABLE IF EXISTS `yl_crm_user`;
CREATE TABLE `yl_crm_user`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL COMMENT '商户ID',
  `customer_id` int(5) NOT NULL COMMENT '商户ID',
  `type_id` int(5) NOT NULL COMMENT '服务类型',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '对应客户',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态 0结束服务 1正常服务',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_dict
-- ----------------------------
DROP TABLE IF EXISTS `yl_dict`;
CREATE TABLE `yl_dict`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `auth_id` int(3) NOT NULL DEFAULT 0 COMMENT '所属权限',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类目名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_dict
-- ----------------------------
INSERT INTO `yl_dict` VALUES (1, 19, '客户管理-行业');
INSERT INTO `yl_dict` VALUES (2, 19, '客户管理-来源');
INSERT INTO `yl_dict` VALUES (3, 19, '客户管理-级别');
INSERT INTO `yl_dict` VALUES (4, 19, '客户管理-状态');
INSERT INTO `yl_dict` VALUES (5, 19, '客户管理-客服类型');
INSERT INTO `yl_dict` VALUES (6, 19, '客户管理-联系人');
INSERT INTO `yl_dict` VALUES (7, 47, '商城模块-品牌管理');

-- ----------------------------
-- Table structure for yl_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `yl_dict_data`;
CREATE TABLE `yl_dict_data`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(3) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `dict_id` int(3) NOT NULL DEFAULT 0 COMMENT '字典ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `sort_order` tinyint(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(10) NOT NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_field
-- ----------------------------
DROP TABLE IF EXISTS `yl_field`;
CREATE TABLE `yl_field`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `auth_id` int(5) NOT NULL DEFAULT 0 COMMENT '绑定权限',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类',
  `default_field` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认字段',
  `disable_field` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '禁用字段名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义字段-分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_field
-- ----------------------------
INSERT INTO `yl_field` VALUES (1, 19, '客户信息-自定义字段', '{\"type\":[\"input\",\"input\",\"dict\",\"dict\",\"dict\",\"input\",\"dict\",\"textarea\",\"shengshi\"],\"title\":[\"\\u5ba2\\u6237\\u540d\\u79f0\",\"\\u5ba2\\u6237\\u7b80\\u79f0\",\"\\u5ba2\\u6237\\u884c\\u4e1a\",\"\\u5ba2\\u6237\\u6765\\u6e90\",\"\\u5ba2\\u6237\\u7ea7\\u522b\",\"\\u8be6\\u7ec6\\u5730\\u5740\",\"\\u5ba2\\u6237\\u72b6\\u6001\",\"\\u5907\\u6ce8\\u4fe1\\u606f\",\"\\u6240\\u5728\\u7701\\u5e02\"],\"name\":[\"name\",\"sname\",\"industry\",\"source\",\"level\",\"address\",\"status\",\"remark\",\"shengshi\"],\"value\":[\"\",\"\",\"1\",\"2\",\"3\",\"\",\"4\",\"\",\"0\"],\"is_sys\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"]}', 'id,shop_id,create_time,update_time,delete_time,name,sname,industry,source,level,province,city,area,address,coo_lat,coo_lng,status,next_time,jdata,user_id,sort_order,remark');
INSERT INTO `yl_field` VALUES (2, 33, '客户回访-自定义字段', '{\"type\":[\"textarea\"],\"title\":[\"\\u56de\\u8bbf\\u5907\\u6ce8\"],\"name\":[\"remark\"],\"value\":[\"\"],\"is_sys\":[\"1\"]}', 'id,shop_id,create_time,update_time,delete_time,jdata,remark,user_id,customer_id');
INSERT INTO `yl_field` VALUES (3, 84, '客户销售-自定义字段', '{\"type\":[\"input\"],\"title\":[\"\\u9500\\u552e\\u5355\\u4ef7\"],\"name\":[\"price\"],\"value\":[\"\"],\"is_sys\":[\"1\"]}', 'id,shop_id,create_time,update_time,delete_time,jdata,price');

-- ----------------------------
-- Table structure for yl_field_data
-- ----------------------------
DROP TABLE IF EXISTS `yl_field_data`;
CREATE TABLE `yl_field_data`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `field_id` int(5) NOT NULL DEFAULT 0 COMMENT '字段分类',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置标题',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置标识',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置类型',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '默认值',
  `options` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项值',
  `sort_order` int(11) NOT NULL DEFAULT 10 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `is_required` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否必填 0否 1是',
  `is_sys` int(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否系统 0否 1是',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_notice
-- ----------------------------
DROP TABLE IF EXISTS `yl_notice`;
CREATE TABLE `yl_notice`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` tinyint(3) NOT NULL DEFAULT 0 COMMENT '通知类型',
  `data` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通知关联内容',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注内容',
  `datetime` int(10) NOT NULL DEFAULT 0 COMMENT '提醒时间',
  `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '状态 0待处理 1已处理',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_shop
-- ----------------------------
DROP TABLE IF EXISTS `yl_shop`;
CREATE TABLE `yl_shop`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商户名称',
  `contact` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `taocan` int(3) NOT NULL DEFAULT 0 COMMENT '套餐ID',
  `admin_id` int(3) NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `exp_time` int(11) NOT NULL DEFAULT 0 COMMENT '过期时间',
  `status` int(3) NOT NULL DEFAULT 1 COMMENT '状态',
  `wechat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '公众号信息',
  `wxapp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '小程序信息',
  `wxpay` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '微信支付',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商家列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_taocan
-- ----------------------------
DROP TABLE IF EXISTS `yl_taocan`;
CREATE TABLE `yl_taocan`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_taocan
-- ----------------------------
INSERT INTO `yl_taocan` VALUES (1, '旗舰版', '超级用户', 1, '1,19,20,21,22,23,29,33,34,36,37,35,30,31,32,38,39,40,41,42,84,85,86,87,83,88,89,90,91,47,48,50,51,52,49,53,54,55,56,66,67,68,69,70,71,72,73,74,75,76,77,57,59,82,60,61,62,63,64,65,58,2,3,6,7,8,4,9,10,11,5,12,13,14,15,16,17,18,24,25,26,27,28,43,44,45,46', 1585059503, 1586151597, 0);
INSERT INTO `yl_taocan` VALUES (2, '营销版', '', 1, '1,71,72,73,74,75,76,77,80,81,57,59,60,61,62,63,64,65,58,2,3,6,7,8,4,9,10,11,5,12,13,14,15,16,17,18,24,25,26,27,28,43,44,45,46', 1585477962, 1585743582, 0);
INSERT INTO `yl_taocan` VALUES (3, 'CRM版', '', 1, '1,19,20,21,22,23,29,33,34,36,37,35,30,31,32,38,39,40,41,42,84,85,86,87,83,2,3,6,7,8,4,9,10,11,5,12,13,14,15,16,17,18,24,25,26,27,28,43,44,45,46', 1585547890, 1585749210, 0);
INSERT INTO `yl_taocan` VALUES (4, '商城版', '', 1, '1,47,48,50,51,52,49,53,54,55,56,57,59,82,60,61,62,63,64,65,58,2,3,6,7,8,4,9,10,11,5,12,13,14,15,16,17,18,24,25,26,27,28,43,44,45,46', 1586053468, 1586053468, 0);

-- ----------------------------
-- Table structure for yl_uploads
-- ----------------------------
DROP TABLE IF EXISTS `yl_uploads`;
CREATE TABLE `yl_uploads`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `shop_id` int(5) NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(5) UNSIGNED NULL DEFAULT 0 COMMENT '用户ID',
  `storage` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储位置',
  `file_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储域名',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `file_size` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `mine` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(3) NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_user
-- ----------------------------
DROP TABLE IF EXISTS `yl_user`;
CREATE TABLE `yl_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` smallint(8) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登陆用户名',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登陆邮箱',
  `mobile` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `avatarurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `sort_order` smallint(5) NULL DEFAULT 0 COMMENT '排序',
  `auth` tinyint(5) NOT NULL DEFAULT 1 COMMENT '个人权限',
  `group_id` tinyint(3) NOT NULL COMMENT '所属部门',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_user_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yl_user_auth_rule`;
CREATE TABLE `yl_user_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yl_user_auth_rule
-- ----------------------------
INSERT INTO `yl_user_auth_rule` VALUES (1, 0, '系统首页', 'user/index/home', 'layui-icon-home', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (2, 0, '员工管理', 'user/staff/index', 'layui-icon-user', 9, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (3, 2, '员工列表', 'user/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (4, 2, '部门设置', 'user/group/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (5, 2, '操作日志', 'user/log/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (6, 3, '添加', 'user/user/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (7, 3, '编辑', 'user/user/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (8, 3, '删除', 'user/user/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (9, 4, '添加', 'user/group/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (10, 4, '编辑', 'user/group/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (11, 4, '删除', 'user/group/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (12, 5, '一键清空', 'user/log/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (13, 0, '系统配置', '', 'layui-icon-set', 10, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (14, 13, '账户配置', 'user/shop/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (15, 13, '字典管理', 'user/dict/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (16, 15, '添加', 'user/dict/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (17, 15, '编辑', 'user/dict/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (18, 15, '删除', 'user/dict/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (19, 0, '客户管理', 'crm/customer/index', 'layui-icon-group', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (20, 19, '客户列表', 'crm/customer/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (21, 20, '新增', 'crm/customer/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (22, 20, '编辑', 'crm/customer/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (23, 20, '删除', 'crm/customer/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (24, 13, '自定义字段', 'user/field/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (25, 24, '添加', 'user/field/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (26, 24, '编辑', 'user/field/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (27, 24, '删除', 'user/field/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (28, 24, '初始化数据', 'user/field/default', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (29, 20, '客户详情', 'crm/detail/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (30, 35, '新增联系人', 'crm/contact/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (31, 35, '编辑联系人', 'crm/contact/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (32, 35, '删除联系人', 'crm/contact/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (33, 20, '回访列表', 'crm/track/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (34, 33, '新增', 'crm/track/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (35, 20, '联系人', 'crm/contact/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (36, 33, '编辑', 'crm/track/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (37, 33, '删除', 'crm/track/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (38, 20, '合同管理', 'crm/contract/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (39, 38, '新增', 'crm/contract/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (40, 38, '编辑', 'crm/contract/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (41, 38, '删除', 'crm/contract/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (42, 38, '合同审核', 'crm/contract/verify', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (43, 13, '提醒设置', 'user/notice/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (44, 43, '新增', 'user/notice/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (45, 43, '修改', 'user/notice/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (46, 43, '删除', 'user/notice/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (47, 0, '商城模块', '', 'layui-icon-cart', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (48, 47, '商品分类', 'shop/category/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (49, 47, '商品列表', 'shop/goods/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (50, 48, '添加', 'shop/category/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (51, 48, '编辑', 'shop/category/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (52, 48, '删除', 'shop/category/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (53, 49, '新增', 'shop/goods/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (54, 49, '编辑', 'shop/goods/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (55, 49, '删除', 'shop/goods/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (56, 47, '订单管理', 'shop/order/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (57, 0, '微信管理', '', 'layui-icon-login-wechat', 8, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (58, 57, '公众号设置', 'wechat/setting/index', '', 10, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (59, 57, '微信用户', 'wechat/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (60, 57, '自定义菜单', 'wechat/menu/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (61, 60, '获取菜单', 'wechat/menu/get_menu', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (62, 60, '发布菜单', 'wechat/menu/send_menu', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (63, 60, '新增', 'wechat/menu/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (64, 60, '编辑', 'wechat/menu/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (65, 60, '删除', 'wechat/menu/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (66, 0, '文章管理', '', 'layui-icon-list', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (67, 66, '栏目管理', 'cms/category/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (68, 67, '新增', 'cms/category/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (69, 67, '编辑', 'cms/category/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (70, 67, '删除', 'cms/category/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (71, 0, '营销系统', '', 'layui-icon-gift', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (72, 71, '红包营销', 'market/hongbao/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (73, 72, '新增', 'market/hongbao/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (74, 72, '编辑', 'market/hongbao/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (75, 72, '删除', 'market/hongbao/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (76, 72, '详情', 'market/hongbao/detail', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (77, 72, '查看', 'market/hongbao/show', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (82, 59, '更新用户', 'wechat/user/get_user', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (83, 19, '统计分析', 'crm/analysis/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (84, 20, '销售管理', 'crm/sales/index', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (85, 84, '新增', 'crm/sales/add', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (86, 84, '编辑', 'crm/sales/edit', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (87, 84, '删除', 'crm/sales/del', '', 0, 'auth', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (88, 83, '客户数据', 'crm/analysis/index', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (90, 83, '合同到期', 'crm/analysis/contract', '', 0, 'nav', 0, 1);
INSERT INTO `yl_user_auth_rule` VALUES (91, 83, '员工统计', 'crm/analysis/user', '', 0, 'nav', 0, 1);

-- ----------------------------
-- Table structure for yl_user_group
-- ----------------------------
DROP TABLE IF EXISTS `yl_user_group`;
CREATE TABLE `yl_user_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` smallint(5) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pid` int(11) NULL DEFAULT 0 COMMENT '上级',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sort_order` int(5) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yl_user_log
-- ----------------------------
DROP TABLE IF EXISTS `yl_user_log`;
CREATE TABLE `yl_user_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` smallint(5) NOT NULL COMMENT '商家ID',
  `user_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `client` smallint(3) NOT NULL DEFAULT 0 COMMENT '客户端',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员日志' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
