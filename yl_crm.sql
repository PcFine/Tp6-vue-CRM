/*
 Navicat Premium Data Transfer

 Source Server         : 本地Mysql
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 127.0.0.1:3306
 Source Schema         : yl_crm

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 30/06/2020 15:05:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `status_id` int(11) NOT NULL COMMENT '销售阶段',
  `next_time` int(11) NOT NULL DEFAULT 0 COMMENT '下次联系时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商机名称',
  `money` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '商机金额',
  `total_price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '产品总金额',
  `deal_date` int(11) NULL DEFAULT NULL COMMENT '预计成交日期',
  `discount_rate` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `create_user_id` int(10) NOT NULL COMMENT '创建人ID',
  `owner_user_id` int(10) NOT NULL COMMENT '负责人ID',
  `ro_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '只读权限',
  `rw_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '读写权限',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business
-- ----------------------------
INSERT INTO `business` VALUES (3, 3, 1, 0, 'Test', 320.00, 320.00, 1593475579, 0.00, '', 1, 1, '', '', 1593421314, 1593475579);
INSERT INTO `business` VALUES (6, 6, 1, 0, 'Test2', 120.00, 120.00, 1593475632, 0.00, '', 1, 1, '', '', 1593475632, 1593475632);

-- ----------------------------
-- Table structure for business_log
-- ----------------------------
DROP TABLE IF EXISTS `business_log`;
CREATE TABLE `business_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL COMMENT '商机id',
  `status_id` int(11) NOT NULL COMMENT '状态id',
  `is_end` tinyint(4) NOT NULL COMMENT '1赢单2输单3无效',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `owner_user_id` int(11) NOT NULL COMMENT '负责人',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机推进日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_log
-- ----------------------------

-- ----------------------------
-- Table structure for business_product
-- ----------------------------
DROP TABLE IF EXISTS `business_product`;
CREATE TABLE `business_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL COMMENT '商机ID',
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '产品单价',
  `sales_price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '销售价格',
  `num` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '数量',
  `discount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '折扣',
  `subtotal` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '小计（折扣后价格）',
  `unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '单位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机产品关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_product
-- ----------------------------
INSERT INTO `business_product` VALUES (5, 6, 3, 120.00, 120.00, 1.00, 0.00, 120.00, '个');

-- ----------------------------
-- Table structure for business_status
-- ----------------------------
DROP TABLE IF EXISTS `business_status`;
CREATE TABLE `business_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机状态名称',
  `rate` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '赢单率',
  `order_id` tinyint(4) NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_status
-- ----------------------------
INSERT INTO `business_status` VALUES (1, '验证客户', '20', 1);
INSERT INTO `business_status` VALUES (2, '需求分析', '15', 2);
INSERT INTO `business_status` VALUES (3, '方案/报价', '30', 3);
INSERT INTO `business_status` VALUES (4, '谈判审核', '30', 4);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `decision` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '是否关键决策人',
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '职务',
  `sex` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `ro_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '只读权限',
  `rw_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '读写权限',
  `create_user_id` int(11) NOT NULL COMMENT '创建人ID',
  `owner_user_id` int(11) NOT NULL COMMENT '负责人ID',
  `next_time` int(11) NOT NULL DEFAULT 0 COMMENT '下次联系时间',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '联系人表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (2, 6, 'Test', '12312', '13695826545', '450897761@qq.com', '是', '123213', '男', '', '', '', '', 1, 1, 0, 1592982720, 1592982878);

-- ----------------------------
-- Table structure for contract
-- ----------------------------
DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `business_id` int(11) NOT NULL COMMENT '商机ID',
  `contacts_id` int(11) NOT NULL COMMENT '客户签约人（联系人ID）',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '合同名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '合同编号',
  `order_date` int(11) NULL DEFAULT NULL COMMENT '下单时间',
  `money` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '合同金额',
  `total_price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '产品总金额',
  `discount_rate` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣',
  `start_time` int(11) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) NULL DEFAULT NULL COMMENT '结束时间',
  `order_user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '公司签约人',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `create_user_id` int(10) NOT NULL COMMENT '创建人ID',
  `owner_user_id` int(10) NOT NULL COMMENT '负责人ID',
  `ro_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '只读权限',
  `rw_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '读写权限',
  `next_time` int(11) NOT NULL DEFAULT 0 COMMENT '下次联系时间',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contract
-- ----------------------------

-- ----------------------------
-- Table structure for contract_pass
-- ----------------------------
DROP TABLE IF EXISTS `contract_pass`;
CREATE TABLE `contract_pass`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NULL DEFAULT NULL COMMENT '合同ID',
  `check_user_array` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核人组 id 用,分隔',
  `check_user_id` int(11) NULL DEFAULT NULL COMMENT '当前操作 审批人ID',
  `status` int(2) NULL DEFAULT NULL COMMENT '审核状态 0待审核、1审核中、2审核通过、3审核未通过、4撤销、5草稿(未提交) 99 撤回审批  100 结束',
  `check_time` int(11) NULL DEFAULT NULL COMMENT '审核时间 -- 时间戳',
  `check_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核备注  驳回备注等',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序列',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contract_pass
-- ----------------------------

-- ----------------------------
-- Table structure for contract_product
-- ----------------------------
DROP TABLE IF EXISTS `contract_product`;
CREATE TABLE `contract_product`  (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL COMMENT '合同ID',
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '产品单价',
  `sales_price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '销售价格',
  `num` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '数量',
  `discount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '折扣',
  `subtotal` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '小计（折扣后价格）',
  `unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '单位',
  PRIMARY KEY (`r_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同产品关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contract_product
-- ----------------------------

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '客户名称',
  `is_lock` int(2) NOT NULL DEFAULT 0 COMMENT '1=锁定 0=未锁定',
  `deal_status` int(2) NOT NULL COMMENT '成交状态 1=已成交 0=未成交',
  `deal_time` int(11) NOT NULL COMMENT '领取，分配，创建时间',
  `level` int(11) NOT NULL COMMENT '客户级别id',
  `industry` int(11) NOT NULL COMMENT '客户行业id',
  `source` int(11) NOT NULL COMMENT '客户来源id',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `create_user_id` int(11) NOT NULL COMMENT '创建人ID',
  `owner_user_id` int(11) NOT NULL COMMENT '负责人ID 无负责人ID就是 公海条件',
  `ro_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '只读权限 适合转移权限时  将原负责人 的权限',
  `rw_user_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '读写权限 适合转移权限时  将原负责人 的权限',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省级',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地市级',
  `district` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县级',
  `lnglat` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地理位置经度',
  `next_time` int(11) NOT NULL DEFAULT 0 COMMENT '下次联系时间',
  `follow` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跟进',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (3, 'Test312', 0, 0, 1592870591, 2, 4, 1, '13695826545', '123123', '450897761@qq.com', '3123', '', 1, 1, '', '', '浙江省温州市永嘉县大若岩镇大若岩镇人民政府', '330000', '330300', '330324', '120.637632,28.28056', 1592877378, '', 1592870591, 1592877378);
INSERT INTO `customer` VALUES (6, 'Test02', 0, 0, 1592902978, 3, 1, 3, '', '', '', '', '', 1, 1, '', '', '', '', '', '', '', 0, '', 1592902978, 1592902978);

-- ----------------------------
-- Table structure for customer_level
-- ----------------------------
DROP TABLE IF EXISTS `customer_level`;
CREATE TABLE `customer_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户级别名称',
  `add_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_level
-- ----------------------------
INSERT INTO `customer_level` VALUES (1, 'A (重点客户)', '2020-06-22 13:46:55');
INSERT INTO `customer_level` VALUES (2, 'B (普通客户)', '2020-06-22 13:45:15');
INSERT INTO `customer_level` VALUES (3, 'C (非优先客户)', '2020-06-22 13:45:15');

-- ----------------------------
-- Table structure for customer_origin
-- ----------------------------
DROP TABLE IF EXISTS `customer_origin`;
CREATE TABLE `customer_origin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源名称',
  `add_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_origin
-- ----------------------------
INSERT INTO `customer_origin` VALUES (1, '促销活动', '2020-06-22 13:44:30');
INSERT INTO `customer_origin` VALUES (2, '搜索引擎', '2020-06-22 13:44:58');
INSERT INTO `customer_origin` VALUES (3, '广告', '2020-06-22 13:45:07');
INSERT INTO `customer_origin` VALUES (4, '转介绍', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (5, '线上注册', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (6, '线上询价', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (7, '预约上门', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (8, '陌拜', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (9, '招商资源', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (10, '公司资源', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (11, '展会资源', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (12, '个人资源', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (13, '电话咨询', '2020-06-22 13:45:15');
INSERT INTO `customer_origin` VALUES (14, '邮件咨询', '2020-06-22 13:45:15');

-- ----------------------------
-- Table structure for customer_trade
-- ----------------------------
DROP TABLE IF EXISTS `customer_trade`;
CREATE TABLE `customer_trade`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户行业',
  `add_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_trade
-- ----------------------------
INSERT INTO `customer_trade` VALUES (1, 'IT/通信/电子/互联网', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (2, '金融业', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (3, '房地产', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (4, '商业服务', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (5, '贸易', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (6, '生产', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (7, '运输/物流', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (8, '服务业', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (9, '文化传媒', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (10, '政府', '2020-06-22 13:49:19');
INSERT INTO `customer_trade` VALUES (99, '其他', '2020-06-22 13:49:19');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '消息类型，用于前端拼接消息',
  `to_user_id` int(10) NOT NULL COMMENT '接收人ID',
  `from_user_id` int(10) NOT NULL COMMENT '发送人ID',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送内容',
  `send_time` int(11) NOT NULL COMMENT '发送时间',
  `read_time` int(11) NOT NULL COMMENT '阅读时间',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块',
  `controller_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制器',
  `action_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法',
  `action_id` int(11) NOT NULL COMMENT '操作ID 对方模块数据表ID',
  PRIMARY KEY (`message_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站内信' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (1, 11, 1, 1, '管理员 提交 test 合同审批待您处理，请及时查看。', 1592815094, 0, 'crm', 'contract', 'save', 1);
INSERT INTO `message` VALUES (2, 11, 2, 1, '管理员 提交 test 合同审批待您处理，请及时查看。', 1593476935, 0, 'crm', 'contract', 'check', 1);
INSERT INTO `message` VALUES (3, 11, 1, 1, '管理员 提交 Test 合同审批待您处理，请及时查看。', 1593477014, 0, 'crm', 'contract', 'save', 2);
INSERT INTO `message` VALUES (4, 11, 1, 1, '管理员 提交 Test 合同审批待您处理，请及时查看。', 1593477049, 0, 'crm', 'contract', 'check', 2);
INSERT INTO `message` VALUES (5, 11, 1, 1, '管理员 提交 Test 合同审批待您处理，请及时查看。', 1593477060, 0, 'crm', 'contract', 'check', 2);
INSERT INTO `message` VALUES (6, 11, 1, 1, '管理员 提交 Test2 合同审批待您处理，请及时查看。', 1593477259, 0, 'crm', 'contract', 'update', 3);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品编码',
  `unit` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '箱' COMMENT '单位',
  `price` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT '标准价格',
  `status` int(2) NOT NULL COMMENT '是否上架 0=未上架 1=上架',
  `category_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品类别',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品描述',
  `create_user_id` int(10) NOT NULL COMMENT '创建人ID',
  `owner_user_id` int(10) NOT NULL COMMENT '负责人ID',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (3, '产品1', '0101', '个', 120.00, 1, '2', '', 1, 1, 1593411153, 1593413570);
INSERT INTO `product` VALUES (4, '产品2', '0102', '块', 200.00, 1, '2', '', 1, 1, 1593411168, 1593411168);

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pid` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES (2, '默认', 0);

-- ----------------------------
-- Table structure for product_file
-- ----------------------------
DROP TABLE IF EXISTS `product_file`;
CREATE TABLE `product_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `file_id` int(11) NOT NULL COMMENT '附件ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品附件关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_file
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
