<?php

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'local'),
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/storage',
            // 磁盘路径对应的外部URL路径
            'url'        => '/storage',
            // 可见性
            'visibility' => 'public',
        ],
        'crm' => [
            'type' => 'local',
            'root' => app()->getRootPath() . 'public/assets/crm',
            'url' => '/assets/crm',
            'visibility' => 'public'
        ],
        'system' => [
            'type' => 'local',
            'root' => app()->getRootPath() . 'public/assets/system',
            'url' => '/assets/system',
            'visibility' => 'public'
        ],
        'project' => [
            'type' => 'local',
            'root' => app()->getRootPath() . 'public/assets/project',
            'url' => '/assets/project',
            'visibility' => 'public'
        ],
        // 更多的磁盘配置信息
    ],
];
