import Vue from 'vue'
import App from './App';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

//引入阿里巴巴第三方库
import '@/assets/icon/alibaba/iconfont.css';

import router from '@/router'
import store from '@/store';
import m_page_list from "@/core/m_page_list"

import init from '@/core/init'


Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.mixin(m_page_list)


import './auth_route';

new Vue({
  render: h => h(App),
  router,
  store,
  beforeCreate: init,
}).$mount('#app')