import NProgress from 'nprogress'
import router from '@/router'
import store from '@/store'
import storage from 'store'
import {
  get_rule,
} from '@/api';

import {
  Message
} from 'element-ui';

NProgress.configure({
  showSpinner: false
})

const whiteList = ['login', 'register', 'registerResult']
const loginRoutePath = '/admin/login'
const defaultRoutePath = '/admin/crm/dashboard'

//开启页面加载进度条
router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  // to.meta && (typeof to.meta.title !== 'undefined' && setDocumentTitle(`${i18nRender(to.meta.title)} - ${domTitle}`))
  /* has token */
  if (storage.get('Auth')) {
    if (to.path === loginRoutePath) {
      next({
        path: defaultRoutePath
      })
      NProgress.done()
    } else {
      // check login user.roles is null
      // console.log(store.getters.rules().length)
      if (store.getters.rules().length === 0) {
        // 请求权限
        get_rule().then(res => {
            const roles = res.rule
            //若无权限 执行退出
            if (roles.length == 0) {
              store.dispatch('login_exit').then(() => {
                next({
                  path: loginRoutePath,
                });
              })
            }
            // console.log(roles)
            //触发权限 并且配置权限
            store.dispatch('init_route', roles).then(() => {
              next({
                ...to,
                // replace: true
              })
              // 请求带有 redirect 重定向时，登录自动重定向到该地址
              // const redirect = decodeURIComponent(from.query.redirect || to.path)
              // if (to.path === redirect) {
              //   // set the replace: true so the navigation will not leave a history record
              //   next({ ...to, replace: true })
              // } else {
              //   // 跳转到目的路由
              //   next({ path: redirect })
              // }
            })
          }).catch(() => {
            Message({
              type: 'error',
              message: '请求用户信息失败，请重试'
            })

            store.dispatch('login_exit').then(() => {
              next({
                path: loginRoutePath,
              });
            })
            // storage.clearAll();
            // 失败时，获取用户信息失败时，调用登出，来清空历史保留信息
            // login_out().then(() => {
            //   next({
            //     path: loginRoutePath,
            //     // query: {
            //     //   redirect: to.fullPath
            //     // }
            //   })
            // })
          })
      } else {
        next()
      }
    }
  } else {
    if (whiteList.includes(to.name)) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next({
        path: loginRoutePath,
        // query: {
        //   redirect: to.fullPath
        // }
      })
      NProgress.done()
    }
  }
})

//关闭页面加载进度条
router.afterEach(() => {
  NProgress.done() // finish progress bar
})