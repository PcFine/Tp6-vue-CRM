//系统管理路由
const system = [{
  path: 'system/depar_user',
  meta: {
    key: 'deparUser',
    parent: 'system',
    type: 'nav',
    status: 1,
    title: '员工与部门',
    icon: 'iconfont icon-depart',
    permission: ['deparUser'],
  },
  component: () => import('@/pages/system/depar_user/index.vue'),
}, {
  path: 'system/group',
  meta: {
    key: 'group',
    parent: 'system',
    type: 'nav',
    status: 1,
    title: '角色管理',
    icon: 'iconfont icon-group',
    permission: ['group'],
  },
  component: () => import('@/pages/system/group/index.vue'),
}, {
  path: 'system/rule',
  meta: {
    key: 'rule',
    parent: 'system',
    type: 'nav',
    status: 1,
    title: '权限信息',
    icon: 'iconfont icon-rule-lock',
    permission: ['rule'],
  },
  component: () => import('@/pages/system/rule/index.vue'),
}, ]

//crm路由
const crm = [{
  path: 'crm/dashboard',
  meta: {
    key: 'dashboard',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '仪表盘',
    icon: 'iconfont icon-dashboard',
    // permission: ['dashboard'],
    permission: [],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'crm/something',
  meta: {
    key: 'something',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '代办事项',
    icon: 'iconfont icon-something',
    // permission: ['something'],
    permission: [],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'crm/customer',
  meta: {
    key: 'customer',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '客户',
    icon: 'iconfont icon-customer',
    permission: ['customer'],
  },
  component: () => import('@/pages/crm/customer/index.vue'),
}, {
  path: 'crm/contacts',
  meta: {
    key: 'contacts',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '联系人',
    icon: 'iconfont icon-contacts',
    permission: ['contacts'],
  },
  component: () => import('@/pages/crm/contacts/index.vue'),
}, {
  path: 'crm/business',
  meta: {
    key: 'business',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '商机',
    icon: 'iconfont icon-business',
    permission: ['business'],
  },
  component: () => import('@/pages/crm/business/index.vue'),
}, {
  path: 'crm/contract',
  meta: {
    key: 'contract',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '合同',
    icon: 'iconfont icon-contract',
    permission: ['contract'],
  },
  component: () => import('@/pages/crm/contract/index.vue'),
}, {
  path: 'crm/receivables',
  meta: {
    key: 'receivables',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '回款',
    icon: 'iconfont icon-receivables',
    permission: ['receivables'],
  },
  component: () => import('@/pages/crm/receivables/index.vue'),
}, {
  path: 'crm/invoice',
  meta: {
    key: 'invoice',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '发票',
    icon: 'iconfont icon-invoice',
    permission: ['invoice'],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'crm/return_visit',
  meta: {
    key: 'returnVisit',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '回访',
    icon: 'iconfont icon-returnVisit',
    permission: ['returnVisit'],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'crm/product',
  meta: {
    key: 'product',
    parent: 'crm',
    type: 'nav',
    status: 1,
    title: '产品',
    icon: 'iconfont icon-product',
    permission: ['product'],
  },
  component: () => import('@/pages/crm/product/index.vue'),
}]

//项目管理路由
const project = [{
  path: 'project/workbench',
  meta: {
    key: 'workbench',
    parent: 'project',
    type: 'nav',
    status: 1,
    title: '工作台',
    icon: 'iconfont icon-workbench',
    permission: [],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'project/project',
  meta: {
    key: 'project',
    parent: 'project',
    type: 'nav',
    status: 1,
    title: '项目',
    icon: 'iconfont icon-project',
    permission: ['project'],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'project/summary',
  meta: {
    key: 'summary',
    parent: 'project',
    type: 'nav',
    status: 1,
    title: '统计分析',
    icon: 'iconfont icon-summary',
    permission: [],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}, {
  path: 'project/archived_items',
  meta: {
    key: 'archivedItems',
    parent: 'project',
    type: 'nav',
    status: 1,
    title: '归档项目',
    icon: 'iconfont icon-archivedItems',
    permission: [],
  },
  component: () => import('@/pages/base/dashboard.vue'),
}]

//基本布局
const layout = [{
  path: '/admin',
  name: 'layout',
  meta: {
    permission: []
  },
  component: () => import("@/layout/admin"),
  redirect: '/admin/crm/dashboard',
  children: [
    ...crm,
    ...project,
    ...system,
  ]
}]


//permission: ['product'], 不为空时  不导出问题

export const ansy_routes = [
  ...layout,
]