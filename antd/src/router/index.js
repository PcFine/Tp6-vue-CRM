import Vue from "vue"
import VueRouter from "vue-router"


Vue.use(VueRouter)

// const admin = [{
//   path: 'dashboard',
//   component: () => import("@/pages/base/dashboard.vue")
// }]

//基本路由
const routes = [{
  path: '/',
  redirect: '/admin/login'
}, {
  path: '/admin/login',
  name:'login',
  component: () => import("@/pages/login.vue")
}, 
// {
//   path: '/admin',
//   name: 'admin',
//   component: () => import("@/layout/admin"),
//   redirect: '/admin/dashboard',
//   children: admin
// }
]





export default new VueRouter({
  routes: routes
})