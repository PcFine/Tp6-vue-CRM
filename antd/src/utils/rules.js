//通用验证规则  一般只涉及 正则验证 和自定义验证

export default {
  //手机
  phone: [{
    pattern: /^0?(10|11|12|13|14|15|16|17|18|19)[0-9]{9}$/g,
    message: '手机格式错误',
    touched: true,
  }],
  //数字
  number: [{
    pattern: /^-?[1-9]\d*$/g,
    message: '请填写正确的数值',
    touched: true,
  }],
  //浮点数
  float: [{
    pattern: /^-?[1-9]*[0-9]?(\.[0-9]*)?$/g,
    message: '请填写正确的浮点数',
    touched: true,
  }],
  //邮箱
  email: [{
    pattern: /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/g,
    message: '邮箱格式错误',
    touched: true,
  }],
  //身份证
  card: [{
    pattern: /^\d{17}[\d|x|X]|\d{16}[\d|x|X]|\d{15}$/g,
    message: '身份证号码格式错误',
    touched: true,
  }],
  //ip地址
  ip: [{
    pattern: /^(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)$/g,
    message: 'ip地址格式错误',
    touched: true,
  }],
  //url地址匹配
  url: [{
    pattern: /^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+$/g,
    message: 'ip地址格式错误',
    touched: true,
  }],
}