//临时文件 


export const menu_module = [{
  id: '1',
  key: 'customer',
  title: '客户管理',
}, {
  id: '2',
  key: 'project',
  title: '项目管理',
}, {
  id: '3',
  key: 'date',
  title: '日历',
}, {
  id: '4',
  key: 'admin',
  title: '系统管理', //超级管理员权限才能进
}]

const customer_menu = [{
  key:'1',
  modo: 'customer',
  title: '仪表盘',
  url: '/admin/home',
}, {
  key:'2',
  modo: 'customer',
  title: '待办事项',
  url: '/admin/home',
}, {
  key:'3',
  modo: 'customer',
  title: '客户',
  url: '/admin/home',
}, {
  key:'4',
  modo: 'customer',
  title: '联系人',
  url: '/admin/home',
}, {
  key:'5',
  modo: 'customer',
  title: '商机',
  url: '/admin/home',
}, {
  key:'6',
  modo: 'customer',
  title: '合同',
  url: '/admin/home',
}, {
  key:'7',
  modo: 'customer',
  title: '回款',
  url: '/admin/home',
}, {
  key:'8',
  modo: 'customer',
  title: '发票',
  url: '/admin/home',
}, {
  key:'9',
  modo: 'customer',
  title: '回访',
  url: '/admin/home',
}, {
  key:'10',
  modo: 'customer',
  title: '产品',
  url: '/admin/home',
}]

const project_menu = [{
  key:'11',
  modo: 'project',
  title: '工作台',
  url: '/admin/home',
}, {
  key:'12',
  modo: 'project',
  title: '项目',
  url: '/admin/home',
}, {
  key:'13',
  modo: 'project',
  title: '统计分析',
  url: '/admin/home',
}, {
  key:'14',
  modo: 'project',
  title: '归档项目',
  url: '/admin/home',
}]

const date_menu = [{
  key:'15',
  modo: 'date',
  title: '我的日历',
  url: '/admin/home',
}]

const admin_menu = [{
  key:'16',
  modo: 'admin',
  title: '员工与部门',
  url: '/admin/home',
}, {
  key:'17',
  modo: 'admin',
  title: '角色管理',
  url: '/admin/home',
}, {
  key:'18',
  modo: 'admin',
  title: '权限管理',
  url: '/admin/home',
}]

export const menu = [
  ...customer_menu,
  ...project_menu,
  ...date_menu,
  ...admin_menu,
]