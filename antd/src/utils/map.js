import AMapLoader from '@amap/amap-jsapi-loader';
import store from '@/store'

export default {
  init(option, call) {
    AMapLoader.load({
      "key": store.state.map.key,
      "version": "1.4.15",
      "plugins": (option.ext || [])
    }).then((AMap) => {
      var map = new AMap.Map('map_container', {
        resizeEnable: true,
      });
      var toolbar = new AMap.ToolBar();
      map.addControl(toolbar);
      call(map);
    }).catch(e => {
      console.log(e);
    })
  }
}