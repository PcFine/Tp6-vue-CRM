export var serialize_params = function (data, type = 'get', url = '') {
  var params = "";
  if (type == 'get') {
    if ((typeof data) == 'string') {
      params = `?${data}`;
    } else {
      for (var n in data) {
        params += `${n}=${data[n]}&`;
      }
      if (/^[^\s]*\?{1}(.)*=/.test(url)) {
        return `&${params.substr(0, params.length - 1)}`;
      } else {
        return `?${params.substr(0, params.length - 1)}`;
      }
    }
  }

  if (type == 'post') {
    if ((typeof data) == 'string') {
      params = data;
    } else {
      for (var n in data) {
        params += `${n}=${data[n]}&`;
      }
    }
    return params.substr(0, params.length - 1);
  }
  return params;
}

