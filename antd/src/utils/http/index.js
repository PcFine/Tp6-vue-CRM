import axios from 'axios'
import store from '@/store'
import router from '@/router'
import {
  MessageBox
} from 'element-ui';



var http = axios.create({
  baseURL: '',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  }
});

http.interceptors.request.use(config => {
  var jwt = store.getters.get_jwt()
  // console.log(jwt)
  if (jwt) {
    config.headers['Auth'] = jwt;
  }
  return config
});


http.interceptors.response.use(res => {
  //身份错误 重新登录
  if (res.data.status == '902') {
    MessageBox.alert(res.data.msg, '提示', {
      confirmButtonText: '确定',
      callback: () => {
        store.dispatch('login_exit').then(() => {
          router.push('/admin/login');
        })
      }
    })
    throw SyntaxError()
  }
  if ((res.headers['auth'] || '') != '') {
    //更新jwt
    // console.log(res.headers)
    store.commit('set_auth', res.headers['auth'])
  }
  return res.data;
});




export default http;