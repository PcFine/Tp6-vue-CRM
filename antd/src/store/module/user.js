import Storage from 'store'
import {
  ansy_routes
} from '@/router/module/route';
import {
  login_out
} from '@/api'

import router from '@/router'

//载入模块
function into_modo(rules) {
  var arr = [];
  for (var n in rules) {
    var node = rules[n];
    arr.push({
      'key': node['code'],
      'parent': node['parent'],
      'code': node['code'],
      'title': node['title'],
      'type': node['type'],
      'status': node['status'],
      'icon': node['icon'],
      'url': node['url'],
    });
  }
  return arr;
}

//载入菜单
function into_nav(rs, rules) {
  var arr = []
  // console.log(rs)
  rs.map(l => {
    // console.log(n)
    //是否需要验证权限
    // console.log(l)
    if (l.name == 'layout') {
      l.children.map(n => {
        if (!(n.meta.permission && n.meta.permission.length)) {
          // if (n.children) {
          //   n.children = into_routes(n.children, rules);
          // }
          arr.push({
            'key': n.meta['key'],
            'parent': n.meta['parent'],
            'code': n.meta['key'],
            'title': n.meta['title'],
            'type': n.meta['type'],
            'status': n.meta['status'],
            'icon': n.meta['icon'],
            'url': n.path,
          })
        } else {
          var nav = null;
          var flag = rules.some(m => {
            return m.children.some(na => {
              nav = na;
              // console.log(na);
              // console.log(n.meta.permission[0])
              return n.meta.permission[0] == na.code;
            })
          })
          if (flag) {
            // console.log(n)
            // if (n.children) {
            //   n.children = into_routes(n.children, rules);
            // }
            arr.push({
              'key': n.meta['key'],
              'parent': n.meta['parent'],
              'code': n.meta['key'],
              'title': n.meta['title'],
              'type': n.meta['type'],
              'status': nav['status'],
              'icon': n.meta['icon'],
              'url': n.path,
            })
            // arr.push(n);
          }
        }
      })
    }
  })
  return arr;
}

//载入路由
function into_routes(rs, rules) {
  var arr = []
  // console.log(routes)
  rs.map(n => {
    // var node = Object.assign(n);
    //深度拷贝 n 元素对象 以防后面操作覆盖n 元素
    var node = {};
    for (var k in n) {
      node[k] = n[k];
    }
    // console.log(node)
    // console.log(n)
    //是否需要验证权限
    if (!(node.meta.permission && node.meta.permission.length)) {
      if (node.children) {
        node.children = into_routes(node.children, rules);
      }
      arr.push(node);
    } else {
      var flag = rules.some(m => {
        return m.children.some(na => {
          return node.meta.permission[0] == na.code;
        })
      })
      if (flag) {
        if (node.children) {
          node.children = into_routes(node.children, rules);
        }
        arr.push(node);
      }
    }
  })
  return arr;
}


const user = {
  //为了 安全起见 这些隐私信息不保存在localstore中以防被篡改
  //利用路由卫士,如果不存在内容 则去后台重新获取后 再跳转
  state: {
    //模块
    modos: [],
    //导航菜单 路由信息
    navs: [],
    //路由模块
    routers: [],
    //权限信息
    rules: [],
    //jwt
    auth: '',
    username: '',
  },
  mutations: {
    set_modos(state, modos) {
      state.modos = modos;
    },
    set_navs(state, navs) {
      state.navs = navs;
    },
    set_rules(state, rules) {
      state.rules = rules;
    },
    set_routes(state, rs) {
      state.routes = rs;
    },
    set_auth(state, auth) {
      state.auth = auth
      Storage.set('Auth', auth);
    },
    set_username(state, name) {
      state.username = name
      Storage.set('UserName', name);
    },
  },
  actions: {
    init_route({
      commit
    }, rules) {
      return new Promise((resolve) => {
        var rule = rules;

        //设置顶部菜单 并排序
        var modo = into_modo(rules.sort((x, y) => {
          if (x.index < y.index) {
            return -1;
          } else if (x.index > y.index) {
            return 1;
          } else {
            return 0;
          }
        }));

        var nav = into_nav(ansy_routes, rules);
        var rs = into_routes(ansy_routes, rules);
        // console.log(rules);
        // console.log(modo);

        commit('set_modos', modo)
        commit('set_navs', nav)
        commit('set_rules', rule)
        commit('set_routes', rs)
        // console.log(nav);
        // console.log(rs)
        router.addRoutes(rs);
        // console.log(router)
        resolve()
      })
    },
    login_successs({
      commit
    }, res) {
      return new Promise((resolve) => {
        commit('set_auth', res.Auth)
        commit('set_username', res.UserName)
        resolve()
      })
    },
    login_exit({
      commit
    }) {
      return new Promise((resolve) => {
        login_out().then(res => {
          commit('set_modos', []);
          commit('set_navs', []);
          commit('set_rules', []);
          commit('set_routes', []);
          Storage.clearAll();
          resolve();
        })
      })
    }
  },
};


export default user