import Storage from 'store'

const page = {
  //为了 安全起见 这些隐私信息不保存在localstore中以防被篡改
  //利用路由卫士,如果不存在内容 则去后台重新获取后 再跳转
  state: {
    //模块
    page: '',
  },
  mutations: {
    set_page(state, val) {
      state.page = val;
      Storage.set('page', val);
    },
  },
  actions: {},
};


export default page