import Vue from 'vue'
import Vuex from 'vuex'
import Storage from 'store'

import user from './module/user'
import page from './module/page';
import map from './module/map'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    map,
    page,
    user,
  },
  state: {},
  mutations: {},
  actions: {},
  getters: {
    get_jwt(state) {
      return () => {
        return state.user.auth
      }
    },
    has_rule(state) {
      //action 格式 modo.nav.action 及 模块.导航菜单.操作事件
      return (action) => {
        var paths = action.split('.')
        // console.log(state.user.rules)
        var flag = false;
        var node = null;

        paths.some(p => {
          // console.log(p)
          // console.log(node)
          if (node) {
            return !node.children.some(r => {
              if (r.code == p) {
                node = r;
                return true;
              }
              return false
            })
          } else {
            return !state.user.rules.some(r => {
              if (r.code == p) {
                node = r;
                return true;
              }
              return false
            })
          }
        })
        if (node) {
          if (node.type == 'action') {
            flag = true;
          }
        }
        return flag;
      }
    },
    rules: (state) => {
      return () => {
        return state.user.rules
      };
    },
    get_page: (state) => {
      return () => {
        return state.page.page;
      }
    },
    get_guid: (state) => {
      return (action) => {
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
        if (action == 'one') {
          guid = guid.replace(/-/g, '');
        }
        return guid;
      }
    }
  },
})