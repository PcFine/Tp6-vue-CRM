import http from '@/utils/http'

import {
  serialize_params
} from '@/utils'


var urls = {
  utils_cate: '/admin/utils/category',
  list: '/admin/business/list',
  demo: '/admin/business/get',
  form: '/admin/business/form',
  del: '/admin/business/del',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "utils_cate") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}