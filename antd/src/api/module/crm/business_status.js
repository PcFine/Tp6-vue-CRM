import http from '@/utils/http'

import {
  serialize_params
} from '@/utils'


var urls = {
  busin_status: '/admin//utils/busin_status',
  list: '/admin/contacts/list',
  demo: '/admin/contacts/get',
  form: '/admin/contacts/form',
  del: '/admin/contacts/del',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == 'busin_status') {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}