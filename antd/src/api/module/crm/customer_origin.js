import http from '@/utils/http'

import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/customer/origin',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}