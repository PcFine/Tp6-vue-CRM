import http from '@/utils/http'

import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/admin/receivables/list',
  demo: '/admin/receivables/get',
  form: '/admin/receivables/form',
  del: '/admin/receivables/del',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}