import http from '@/utils/http'

import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/admin/product/list',
  demo: '/admin/product/get',
  form: '/admin/product/form',
  del: '/admin/product/del',
  file_del: '/admin/product/file_del'
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "file_del") {
    method = "post";
    data = serialize_params(option, method);
  }

  return http({
    url: url,
    method: method,
    data: data,
  })
}