import http from '@/utils/http'
import {
  serialize_params
} from '@/utils'


var urls = {
  login: '/admin/login',
  login_out: '/admin/login_out',
  get_rule: '/admin/get_rule'
}


const login = (option) => {
  return http({
    url: urls.login,
    method: 'post',
    data: serialize_params(option, 'post'),
  })
}


const login_out = () => {
  return http({
    url: urls.login_out,
    method: 'post',
  })
}

const get_rule = (option) => {
  return http({
    url: urls.get_rule,
    method: 'get',
  })
}


export {
  login,
  login_out,
  get_rule,
}