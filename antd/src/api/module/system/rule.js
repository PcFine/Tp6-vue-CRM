import http from '@/utils/http'
import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/admin/rule/list',
  form: '/admin/rule/form',
  demo: '/admin/rule/get',
  del: '/admin/rule/del',
  rule_type: '/admin/rule_type',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "rule_type") {
    method = "get";
    // url += serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}