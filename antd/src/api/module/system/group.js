import http from '@/utils/http'
import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/admin/group/list',
  form: '/admin/group/form',
  demo: '/admin/group/get',
  del: '/admin/group/del',
  get_rule: '/admin/group/get_rule',
  set_rule: '/admin/group/set_rule',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "get_rule") {
    method = "get";
    url += serialize_params(option, method);
    // data = serialize_params(option, method);
  }
  if (type == "set_rule") {
    method = "post";
    // url += serialize_params(option, method);
    data = serialize_params(option, method);
  }

  return http({
    url: url,
    method: method,
    data: data,
  })
}