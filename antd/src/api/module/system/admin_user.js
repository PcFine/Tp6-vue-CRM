import http from '@/utils/http'
import {
  serialize_params
} from '@/utils'


var urls = {
  list: '/admin/user/list',
  form: '/admin/user/form',
  demo: '/admin/user/get',
  del: '/admin/user/del',
  get_rule: '/admin/user/get_rule',
  set_rule: '/admin/user/set_rule',
}

export default (type, option) => {
  var url = urls[type];
  var method = "get";
  var data = "";
  if (type == "list") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "form") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "demo") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "del") {
    method = "post";
    data = serialize_params(option, method);
  }
  if (type == "get_rule") {
    method = "get";
    url += serialize_params(option, method);
  }
  if (type == "set_rule") {
    method = "post";
    data = serialize_params(option, method);
  }
  return http({
    url: url,
    method: method,
    data: data,
  })
}