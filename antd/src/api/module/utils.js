import http from '@/utils/http'
import {
  serialize_params
} from '@/utils'

var urls = {
  'upload': '/admin/upload'
}


export const upload = (option) => {
  var F = new FormData();
  for (var n in option) {
    F.append(n, option[n]);
  }
  return http({
    method: 'post',
    url: urls.upload,
    headers: {
      "Content-Type": "multipart/form-data"
    },
    data: F,
  })
}