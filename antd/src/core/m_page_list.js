import moment from 'moment'

export default {
  data() {
    return {
      defualt: {
        rules: {
          is_null: [{
            required: true,
            message: '请输入内容',
            trigger: 'blur'
          }],
          email: [{
            pattern: /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/g,
            message: '格式错误',
            trigger: 'blur'
          }],
          telephone: [{
            pattern: /^0?(10|11|12|13|14|15|16|17|18|19)[0-9]{9}$/g,
            message: '格式错误',
            trigger: 'blur'
          }],
          number: function (action = '', min = 0, max = 100) {
            return [{
              validator: (r, v, c) => {
                if (v == null) {
                  c(new Error('请输入数值'));
                  return false;
                }

                if (v.toString() == '') {
                  c(new Error('请输入数值'));
                  return false;
                }

                var num = Number(v);
                if (num.toString() == 'NaN') {
                  c(new Error('请输入数值'));
                  return false;
                }

                if (action == 'gt') {
                  if (num <= min) {
                    c(new Error(`请输入数值,大于${min}`));
                    return false;
                  }
                }

                if (action == 'gt_eq') {
                  if (num < min) {
                    c(new Error(`请输入数值,大于等于${min}`));
                    return false;
                  }
                }

                if (action == 'lt') {
                  if (num > max) {
                    c(new Error(`请输入数值,小于${min}`));
                    return false;
                  }
                }
                if (action == 'lt_eq') {
                  if (num >= max) {
                    c(new Error(`请输入数值,小于等于${min}`));
                    return false;
                  }
                }
                c();
              },
              trigger: 'blur'
            }]
          },
          card: [{
            pattern: /^([0-9]){15,18}(x|X)?$/g,
            message: '格式错误',
            trigger: 'blur'
          }],
          //用户账号格式
          username: [{
            pattern: /^[a-zA-Z][a-zA-Z0-9_]{4,15}$/g,
            message: '格式错误',
            trigger: 'blur',
          }],
          //ip地址
          ip: [{
            pattern: /^(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)$/g,
            message: '格式错误',
            trigger: 'blur'
          }]
        }
      }
    }
  },
  methods: {
    delay_for_fun(func, ms) {
      setTimeout(() => {
        if (!func()) {
          this.delay_for_fun(func, ms);
        }
      }, ms);
    },
  },
  computed: {
    hasRule() {
      var _self = this;
      return (action) => {
        return _self.$store.getters.has_rule(action)
      }
    },
    optionKey() {
      var _self = this
      return (key, def, obj) => {
        if (obj) {
          return (obj && obj[key]) == undefined ? def : obj[key]
        }
        return (_self.option && _self.option[key]) == undefined ? def : _self.option[key]
      }
    },
    date_format() {
      var _self = this;
      return (time, format) => {
        if (time == '' || time == 0) {
          return ''
        }
        if (typeof time == 'number') {
          if (time.toString().length <= 10) {
            return moment(time * 1000).format(format);
          }
        }
        return moment(time).format(format);
      }
    },
    guid() {
      var _self = this
      return (action) => {
        return _self.$store.getters.get_guid(action);
      }
    }
  },
}