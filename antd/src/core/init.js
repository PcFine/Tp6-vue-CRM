import Storage from 'store'
import store from '@/store'


export default function () {
  store.commit('set_page', Storage.get('page'));
  store.commit('set_auth', Storage.get('Auth'));
  store.commit('set_username', Storage.get('UserName'));
}