module.exports = {
  devServer: {
    host: '127.0.0.1',
    port: '8080',
    proxy: 'http://127.0.0.1:8000'
  },
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = '后台管理'
        return args
      })
  }
}