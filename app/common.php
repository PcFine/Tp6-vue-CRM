<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: sueRimn
 * @Date: 2020-05-05 19:26:53
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-05-05 21:22:08
 */
// 应用公共文件

use app\admin\model\system\AdminUserModel;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use think\facade\Db;

//获取当前登录用户  -- 进行更新 -读取缓存信息
function user()
{
  $user = null;
  $auth_key = request()->header('Auth', '');
  $black = cache('auth_key_black');

  //验证是否存在黑名单
  if (is_array($black ?? '')) {
    $isExist =  array_filter($black, function ($n) use ($auth_key) {
      return $auth_key == $n;
    });
    if (count($isExist) > 0) {
      return null;
    }
  }

  if ($auth_key != '') {
    $cache = cache($auth_key);
    if ($cache) {
      $user = AdminUserModel::where('id', $cache['user_id'])->find();
      if ($user) {
        $user->group;
        $user->rule;
      }
    }
  }

  return $user;
}

//生成msg响应json
function res_msg($text = '', $status = '200')
{
  $msg = [
    'status' => '200',
    'msg' => '',
  ];
  if ($msg != '') {
    $msg['status'] = $status;
    $msg['msg'] = $text;
  }
  return $msg;
}

//创建jwt
function create_jwt($data)
{
  $data['exp'] = time() + config('app.jwt_exp');
  $token = JWT::encode($data, config('app.jwt_key'));
  return $token;
}

//解析jwt
function ansy_jwt($jwt)
{
  $black = cache('jwt_black');

  //验证是否存在黑名单
  if (is_array($black ?? '')) {
    $isExist =  array_filter($black, function ($n) use ($jwt) {
      return $jwt == $n;
    });
    if (count($isExist) > 0) {
      return false;
    }
  }

  // cache('jwt_black', [], config('app.jwt_exp', 3600));

  try {
    $arr = JWT::decode($jwt, config('app.jwt_key'), array('HS256'));
    return $arr;
  } catch (SignatureInvalidException $e) {
    return false;
  } catch (BeforeValidException $e) {
    return false;
  } catch (ExpiredException $e) {
    return false;
  } catch (Exception $e) {
    return false;
  }
}

//加密密码
function create_pwd($pwd, $iv = "yalong_123456789")
{
  $data = openssl_encrypt($pwd, 'AES-128-CBC', config('app.pwd_key'), OPENSSL_RAW_DATA, $iv);
  $data = strtolower(bin2hex($data));
  return $data;
}

//生成guid()
function guid()
{
  if (function_exists('com_create_guid')) {
    return com_create_guid();
  } else {
    mt_srand((float) microtime() * 10000); //optional for php 4.2.0 and up.
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45); // "-"
    // $uuid = chr(123) // "{"
    //   . substr($charid, 0, 8) . $hyphen
    //   . substr($charid, 8, 4) . $hyphen
    //   . substr($charid, 12, 4) . $hyphen
    //   . substr($charid, 16, 4) . $hyphen
    //   . substr($charid, 20, 12)
    //   . chr(125); // "}"
    $uuid =  substr($charid, 0, 8) . $hyphen
      . substr($charid, 8, 4) . $hyphen
      . substr($charid, 12, 4) . $hyphen
      . substr($charid, 16, 4) . $hyphen
      . substr($charid, 20, 12);
    return $uuid;
  }
}

//插入jwt黑名单
function in_jwt_black($jwt)
{
  $arr = [];
  if (is_array((cache('jwt_black') ?? ''))) {
    $arr = cache('jwt_black');
  }
  array_push($arr, $jwt);
  // cache('jwt_black', $arr, config('app.jwt_exp', 1800));
  cache('jwt_black', $arr, 12 * 60 * 60);
}


//将auth_key加入黑名单
function in_auth_key_black($auth_key)
{
  $arr = [];
  if (is_array((cache('auth_key_black') ?? ''))) {
    $arr = cache('auth_key_black');
  }
  array_push($arr, $auth_key);
  // cache('jwt_black', $arr, config('app.jwt_exp', 1800));
  cache('auth_key_black', $arr, 12 * 60 * 60);
}

//验证白名单
function ansy_white($request)
{
  $path = 'admin.' . '.' . strtolower($request->controller) . '.' . strtolower($request->action);
  return in_array($path, config('app.white'));
}


//获取权限信息 获取权限时 从缓存中获取
function rules($user, $cache = true)
{
  $temp_rules = [];
  //从缓存中获取权限缓存
  if ($cache) {
    if ($user->auth_key != '') {
      $val = cache($user->auth_key);
      if ($val) {
        if (is_array($val)) {
          if (array_key_exists('rule', $val)) {
            $rules = $val['rule'];
            return $rules;
          }
        }
      }
    }
  }
  //获取权限信息
  $rule = $user->rule;
  foreach ($rule as $n) {
    #获取顶级菜单
    $n->parent->parent;
    //判定元素是否存在
    $f = array_filter($temp_rules, function ($x) use ($n) {
      return $x->id == $n->id;
    });
    if (count($f) == 0) {
      array_push($temp_rules, $n);
    }
  };

  //获取角色权限信息信息
  $group = $user->group;
  foreach ($group as $n) {
    $g_rule = $n->rule;
    foreach ($g_rule as $r) {
      #获取顶级菜单
      $r->parent->parent;
      //判定元素是否存在
      $f = array_filter($temp_rules, function ($x) use ($r) {
        return $x->id == $r->id;
      });
      if (count($f) == 0) {
        array_push($temp_rules, $r);
      }
    }
  };
  //拼接id 字符串 用以查询权限列表
  $ids = '';
  foreach ($temp_rules as $n) {
    $ids = $ids . "'" . $n->id . "',";
  }
  $ids = ($ids != '' ? substr($ids, 0, strlen($ids) - 1) : "''");

  //获取列表后 重新规整权限成树状图
  $rules = [];

  function filter(&$arr, $n, $path = "", $type = "modo")
  {
    $str = explode('.', $path);
    if (count($str) > 0) {
      $item = null;
      for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i]['code'] == $str[0]) {
          $item = &$arr[$i];
          break;
        }
      }
      if (!$item) {
        $obj = $n;
        if ($type == 'modo') {
          $obj = $n->parent->parent;
        }
        if ($type == 'nav') {
          $obj = $n->parent;
        }
        if ($type == 'action') {
          $obj = $n;
        }
        $item = [
          'code' => $obj->code,
          'parent' => $obj->parent_code,
          'title' => $obj->name,
          'type' => $obj->type,
          'status' => $obj->status,
          'url' => $obj->url,
          'icon' => $obj->icon,
          'index' => $obj->index,
        ];
        if ($type != 'action') {
          $item['children'] = [];
        }
        $sum = array_push($arr, $item);
        $item = &$arr[$sum - 1];
      }
      if ($type == 'modo') {
        filter($item['children'], $n, $str[1] . '.' . $str[2], 'nav');
      }
      if ($type == 'nav') {
        filter($item['children'], $n, $str[1], 'action');
      }
    }
    // return $arr;
  }

  foreach ($temp_rules as $n) {
    // halt($n);
    filter($rules, $n, $n->parent->parent->code . '.' . $n->parent->code . '.' . $n->code);
  }
  return $rules;
}


//获取客户端IP地址
function get_client_ip($type = 0, $adv = false)
{
  $type       =  $type ? 1 : 0;
  static $ip  =   NULL;
  if ($ip !== NULL) return $ip[$type];
  if ($adv) {
    //nginx 代理模式下，获取客户端真实IP
    //浏览当前页面的用户计算机的网关
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
      $pos    =   array_search('unknown', $arr);
      if (false !== $pos) unset($arr[$pos]);
      $ip     =   trim($arr[0]);
      //客户端的ip
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
      $ip     =   $_SERVER['HTTP_CLIENT_IP'];
      //浏览当前页面的用户计算机的ip地址
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
      $ip     =   $_SERVER['REMOTE_ADDR'];
    }
  } elseif (isset($_SERVER['REMOTE_ADDR'])) {
    $ip     =   $_SERVER['REMOTE_ADDR'];
  }
  // IP地址合法验证
  $long = sprintf("%u", ip2long($ip));
  $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
  return $ip[$type];
}


//读取数据库信息
function database($key)
{
  $arr = config('database');
  if (count($arr) > 0) {
    $conn = $arr['connections'];
    if (isset($conn[$key])) {
      return $conn[$key];
    }else{
      return null;
    }
  } else {
    return null;
  }
}
