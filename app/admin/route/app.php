<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: sueRimn
 * @Date: 2020-05-05 20:10:29
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-05-05 21:21:06
 */

use think\facade\Route;

// Route::get('/', 'index');

//中间件 验证权限  登录等信息
$midd_login = [
  //验证登录
  \app\admin\middleware\AuthLogin::class,
  //验证jwt 已废弃 利用guid 配合 redis缓存 实现前后端分离 模仿session持久化会话
  // \app\admin\middleware\AuthJwt::class,
];

$midd_rule = [
  //验证权限
  \app\admin\middleware\AuthRule::class,
];

//admin模块 基本页面 如登录 退出等操作路由
Route::group(function () {
  //登录退出等
  Route::post('/index', 'system.index/index');
  Route::post('/login', 'system.index/login');
  Route::post('/login_out', 'system.index/login_out');
});

//基本操作模块
//基本操作路由
Route::group(function () {
  Route::get('/get_rule', 'system.index/get_rule');
  Route::get('/rule_type', 'system.rule/rule_type');
  Route::post('/upload', 'system.file/upload');

  Route::get('/utils/category', 'crm.productcate/index');
  Route::get('/utils/busin_status', 'crm.businessstatus/index');
})->middleware($midd_login);

//admin模块 操作页面路由
Route::group(function () {

  //操作员表路由
  Route::group('/user', function () {
    Route::get('/list', 'system.adminuser/index');
    Route::post('/form', 'system.adminuser/form');
    Route::get('/get', 'system.adminuser/get');
    Route::post('/del', 'system.adminuser/del');
    Route::get('/get_rule', 'system.adminuser/get_rule');
    Route::post('/set_rule', 'system.adminuser/set_rule');
  });

  //部门路由
  Route::group('/department', function () {
    Route::get('/list', 'system.department/index');
    Route::post('/form', 'system.department/form');
    Route::get('/get', 'system.department/get');
    Route::post('/del', 'system.department/del');
  });

  // 权限表路由
  Route::group('/rule', function () {
    Route::get('/list', 'system.rule/index');
    Route::post('/form', 'system.rule/form');
    Route::get('/get', 'system.rule/get');
    Route::post('/del', 'system.rule/del');
  });

  //角色表路由
  Route::group('/group', function () {
    Route::get('/list', 'system.group/index');
    Route::post('/form', 'system.group/form');
    Route::get('/get', 'system.group/get');
    Route::post('/del', 'system.group/del');
    Route::get('/get_rule', 'system.group/get_rule');
    Route::post('/set_rule', 'system.group/set_rule');
  });


  //客户信息表
  Route::group('/customer', function () {
    Route::get('/list', 'crm.customer/index');
    Route::post('/form', 'crm.customer/form');
    Route::get('/get', 'crm.customer/get');
    Route::post('/del', 'crm.customer/del');
  });

  //联系人信息表
  Route::group('/contacts', function () {
    Route::get('/list', 'crm.contacts/index');
    Route::post('/form', 'crm.contacts/form');
    Route::get('/get', 'crm.contacts/get');
    Route::post('/del', 'crm.contacts/del');
  });

  //商机信息表
  Route::group('/business', function () {
    Route::get('/list', 'crm.business/index');
    Route::post('/form', 'crm.business/form');
    Route::get('/get', 'crm.business/get');
    Route::post('/del', 'crm.business/del');
  });

  //合同信息表
  Route::group('/contract', function () {
    Route::get('/list', 'crm.contract/index');
    Route::post('/form', 'crm.contract/form');
    Route::get('/get', 'crm.contract/get');
    Route::post('/del', 'crm.contract/del');
  });

  //回款信息表
  Route::group('/receivables', function () {
    Route::get('/list', 'crm.receivables/index');
    Route::post('/form', 'crm.receivables/form');
    Route::get('/get', 'crm.receivables/get');
    Route::post('/del', 'crm.receivables/del');
  });

  //产品信息表
  Route::group('/product', function () {
    Route::get('/list', 'crm.product/index');
    Route::post('/form', 'crm.product/form');
    Route::get('/get', 'crm.product/get');
    Route::post('/del', 'crm.product/del');
    Route::post('/file_del', 'crm.product/file_del');
  });
})->middleware(array_merge($midd_login, $midd_rule));
