<?php
declare (strict_types = 1);

namespace app\admin\model\system;

use app\admin\model\BaseModel;

/**
 * 文件模型表
 */
class FileModel extends BaseModel
{
    protected $connection = 'system';
    protected $table = 'files';
}
