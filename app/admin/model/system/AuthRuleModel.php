<?php

namespace app\admin\model\system;

use app\admin\model\BaseModel;

/**
 * 权限模型
 */
class AuthRuleModel extends BaseModel
{
  protected $connection = 'system';
  protected $table = 'auth_rule';

  public function parent()
  {
    return $this->hasOne(AuthRuleModel::class, "id", "parent_id");
  }
}
