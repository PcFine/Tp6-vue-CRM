<?php

declare(strict_types=1);

namespace app\admin\model\system;

use app\admin\model\BaseModel;

/**
 * 部门模型
 */
class DepartmentModel extends BaseModel
{
    protected $connection = 'system';
    protected $table = 'department';
}
