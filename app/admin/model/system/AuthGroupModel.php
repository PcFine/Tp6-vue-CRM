<?php

namespace app\admin\model\system;

use app\admin\model\BaseModel;

/**
 * 角色模型
 */
class AuthGroupModel extends BaseModel
{
  protected $connection = 'system';
  protected $table = 'auth_group';
  
  public function rule()
  {
    return $this->belongsToMany(AuthRuleModel::class, 'auth_group_rule', 'rule_id', 'group_id');
  }
}
