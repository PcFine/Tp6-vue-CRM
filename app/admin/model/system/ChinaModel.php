<?php

declare(strict_types=1);

namespace app\admin\model\system;

use app\admin\model\BaseModel;
use think\Model;

/**
 * 中国省份城市模型
 */
class ChinaModel extends BaseModel
{
    protected $connection = 'system';
    protected $table = 'china';
}
