<?php

namespace app\admin\model\system;

use app\admin\model\BaseModel;
use think\Model;

/**
 * 操作员及员工模型
 */
class AdminUserModel extends BaseModel
{
  protected $connection = 'system';
  protected $table = 'admin_user';

  public function rule()
  {
    return $this->belongsToMany(AuthRuleModel::class, 'auth_user_rule', 'rule_id', 'user_id');
  }

  public function group()
  {
    return $this->belongsToMany(AuthGroupModel::class, 'auth_user_group', 'group_id', 'user_id');
  }

  public function check_pwd($pwd)
  {
    // halt($this->password);
    return $this->password == create_pwd($pwd);
  }
}
