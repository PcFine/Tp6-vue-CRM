<?php

namespace app\admin\model;

use think\Model;

/**
 * 操作员及员工模型
 */
class BaseModel extends Model
{
  public function __construct(array $data = [])
  {
    $config =  database($this->getConnection());
    $database = '';
    if ($config) {
      $database = $config['database'] . '.';
    }
    $this->table = $database . $this->table;
    parent::__construct($data);
  }
}
