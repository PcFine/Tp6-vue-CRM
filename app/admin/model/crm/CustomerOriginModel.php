<?php
declare (strict_types = 1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 客户源模型
 */
class CustomerOriginModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'customer_origin';
}
