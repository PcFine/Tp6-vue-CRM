<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 联系人模型
 */
class ContactsModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'contacts';

    public function customer()
    {
       return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }
}
