<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 商机模型
 */
class BusinessModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'business';


    public function customer()
    {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }

    public function status()
    {
        return $this->hasOne(BusinessStatusModel::class, 'id', 'status_id');
    }

    public function product()
    {
        $config =  database($this->getConnection());
        $database = '';
        if ($config) {
            $database = $config['database'] . '.';
        }
        return $this->belongsToMany(ProductModel::class, $database . 'business_product', 'product_id', 'business_id');
    }
}
