<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * @mixin think\Model
 */
class ReceivablesModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'receivables';

    public function customer()
    {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }

    public function contract()
    {
        return $this->hasOne(ContractModel::class, 'id', 'contract_id');
    }

    public function pass()
    {
        return $this->hasMany(ReceivablesPassModel::class, 'receivables_id', 'id');
    }
}
