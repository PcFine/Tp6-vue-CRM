<?php
declare (strict_types = 1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 客户等级模型
 */
class CustomerLevelModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'customer_level';
}
