<?php
declare (strict_types = 1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;
use think\Model;

/**
 * @mixin think\Model
 */
class BusinessStatusModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'business_status';
}
