<?php
declare (strict_types = 1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;
use app\admin\model\system\AdminUserModel;
/**
 * @mixin think\Model
 */
class ReceivablesPassModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'receivables_pass';

    public function checkuser()
    {
        return $this->hasOne(AdminUserModel::class, 'id', 'check_user_id');
    }

    public function passuser()
    {
        return $this->hasOne(AdminUserModel::class, 'id', 'pass_user_id');
    }
}
