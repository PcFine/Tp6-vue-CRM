<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 产品分类模型
 */
class ProductCateModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'product_category';
}
