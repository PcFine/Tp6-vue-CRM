<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;
use app\admin\model\system\AdminUserModel;

/**
 * 合同审批流程 模型
 */
class ContractPassModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'contract_pass';

    public function checkuser()
    {
        return $this->hasOne(AdminUserModel::class, 'id', 'check_user_id');
    }

    public function passuser()
    {
        return $this->hasOne(AdminUserModel::class, 'id', 'pass_user_id');
    }
}
