<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;
use app\admin\model\system\FileModel;
use think\Model;

/**
 * 产品模型
 */
class ProductModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'product';

    public function files()
    {
        $config =  database($this->getConnection());
        $database = '';
        if ($config) {
            $database = $config['database'] . '.';
        }
        return $this->belongsToMany(FileModel::class, $database . 'product_file', 'file_id', 'product_id');
    }

    public function category()
    {
        return $this->hasOne(ProductCateModel::class,  'id', 'category_id');
    }
}
