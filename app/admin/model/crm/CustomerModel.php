<?php

declare(strict_types=1);

namespace app\admin\model\crm;

// use app\admin\controller\crm\CustomerLevel;
// use app\admin\controller\crm\CustomerOrigin;
// use app\admin\controller\crm\CustomerTrade;

use app\admin\model\BaseModel;
use app\admin\model\system\ChinaModel;

/**
 * 客户模型
 */
class CustomerModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'customer';


    public function levels()
    {
        return $this->hasOne(CustomerLevelModel::class, 'id', 'level');
    }

    public function industrys()
    {
        return $this->hasOne(CustomerTradeModel::class, 'id', 'industry');
    }

    public function sources()
    {
        return $this->hasOne(CustomerOriginModel::class, 'id', 'source');
    }

    public function provinces()
    {
        return $this->hasOne(ChinaModel::class, 'adcode', 'province');
    }

    public function citys()
    {
        return $this->hasOne(ChinaModel::class, 'adcode', 'city');
    }

    public function districts()
    {
        return $this->hasOne(ChinaModel::class, 'adcode', 'district');
    }
}
