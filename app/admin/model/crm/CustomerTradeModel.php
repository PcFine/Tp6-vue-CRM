<?php

declare(strict_types=1);

namespace app\admin\model\crm;

use app\admin\model\BaseModel;

/**
 * 客户行业模型
 */
class CustomerTradeModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'customer_trade';
}
