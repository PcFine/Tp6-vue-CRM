<?php

declare(strict_types=1);

namespace app\admin\model\crm;
use app\admin\model\system\AdminUserModel;
use app\admin\model\BaseModel;

/** 
 * 合同模型
 */
class ContractModel extends BaseModel
{
    protected $connection = 'crm';
    protected $table = 'contract';

    public function customer()
    {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }

    public function business()
    {
        return $this->hasOne(BusinessModel::class, 'id', 'business_id');
    }

    public function contacts()
    {
        return $this->hasOne(ContactsModel::class, 'id', 'contacts_id');
    }

    public function orderuser()
    {   
        return $this->hasOne(AdminUserModel::class, 'id', 'order_user_id');
    }

    public function product()
    {
        $config =  database($this->getConnection());
        $database = '';
        if ($config) {
            $database = $config['database'] . '.';
        }
        return $this->belongsToMany(ProductModel::class, $database . 'contract_product', 'product_id', 'contract_id');
    }

    public function pass()
    {
        return $this->hasMany(ContractPassModel::class, 'contract_id', 'id');
    }
}
