<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\ContractModel;
use app\admin\model\system\AdminUserModel;
use app\admin\validate\crm\ContractValidate;
use think\exception\ValidateException;

/** 
 * 后台-合同-控制器
 */
class Contract
{

    public $rule = [
        'index' => 'crm.contract.view',
        'form' => 'crm.contract.add',
        'get' => 'crm.contract.edit',
        'del' => 'crm.contract.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $customer = input('customer', '');
        $is_pass = input('pass', '0');

        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }

        if ($customer != '') {
            array_push($where, ["customer_id", '=', $customer]);
        }

        if ($is_pass == '1') {
            //取合同已审核的
        }


        $obj = ContractModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->customer;
            $o->business;
            $o->contacts;
            $o->pass;
            $o->order_user;
            $o->pass;
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'code' => input('code', ''),
            'name' => input('name', ''),
            'customer_id' => input('customer', ''),
            'business_id' => input('business', ''),
            'contacts_id' => input('contacts', ''),
            'money' => input("money", ''),
            'total_price' => input("total_price", 0),
            'order_date' => input("order_date", 0),
            'start_time' => input("start_time", 0),
            'end_time' => input("end_time", 0),
            'discount_rate' => input("discount_rate", 0),
            'order_user_id' => input('order_user', 0),
            'remark' => input("memo", ''),
        ];

        if ($data['order_date'] != 0 || $data['order_date'] != '') {
            $data['order_date'] = strtotime($data['order_date']);
        }

        if ($data['start_time'] != 0 || $data['start_time'] != '') {
            $data['start_time'] = strtotime($data['start_time']);
        }

        if ($data['end_time'] != 0 || $data['end_time'] != '') {
            $data['end_time'] = strtotime($data['end_time']);
        }
        //验证内容
        try {
            validate(ContractValidate::class)->check([
                'id' => input('id', ''),
                'check_user' => input('check_user', '')
            ] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["next_time"] = 0;
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;
            $data["ro_user_id"] = '';
            $data["rw_user_id"] = '';

            $m = ContractModel::create($data);

            $msg['id'] = $m->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $m = ContractModel::where('id', input('id', ''))->find();
            $data["update_time"] = time();
            $m->save($data);
            $msg['msg'] = "修改成功";
        }

        // 添加审批人 记得写入message 表
        if (count($m->pass) < 1 && input('check_user', '') != '') {
            $m->pass()->where([])->delete();
            $m->pass()->save([
                'check_user_id' => input('check_user', ''),
                'status' => 0,
                'check_time' => 0,
                'check_remark' => '',
                'order' => 1,
            ]);
        }

        $product = json_decode(input('product', ''));
        $m->product()->detach();
        if (count($product) > 0) {
            foreach ($product as $p) {
                $m->product()->attach($p->id, [
                    'price' => $p->price,
                    'sales_price' => $p->sales_price,
                    'num' => $p->num,
                    'discount' => $p->discount,
                    'subtotal' => $p->subtotal,
                    'unit' => $p->unit,
                ]);
            }
        }
        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $modo = ContractModel::where(["id" => $id])->find();
        $modo->customer;
        $modo->business;
        $modo->contacts;
        $modo->orderuser;
        foreach ($modo->pass as $p) {
            $p->checkuser;
        }
        foreach ($modo->product as $p) {
            $p->category;
        }
        $msg['modo'] = $modo;
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));

        //检测关联

        $models = ContractModel::where([['id', 'in', $ids]])->select();
        foreach ($models as $m) {
            $m->product()->detach();
            $m->pass()->where([])->delete();
        }

        ContractModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
