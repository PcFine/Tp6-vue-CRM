<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\ReceivablesModel;
use app\admin\validate\crm\ReceivablesValidate;
use think\exception\ValidateException;

class Receivables
{

    public $rule = [
        'index' => 'crm.receivables.view',
        'form' => 'crm.receivables.add',
        'get' => 'crm.receivables.edit',
        'del' => 'crm.receivables.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $p_code = input('p_code', '');
        $customer = input('customer', '');
        $is_pass = input('pass', '0');

        $where = [];
        if ($key != '') {
            array_push($where, ["code", 'like', '%' . $key . '%']);
        }

        if ($p_code != '') {
            array_push($where, ["p_code", '=', $p_code]);
        }

        if ($customer != '') {
            array_push($where, ["customer_id", '=', $customer]);
        }

        if ($is_pass == '1') {
            //取合同已审核的
        }


        $obj = ReceivablesModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->customer;
            $o->contract;
            $o->pass;
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'code' => input('code', ''),
            'customer_id' => input('customer', ''),
            'contract_id' => input('contract', ''),
            'return_type' => input('return_type', ''),
            'return_time' => input('return_time', ''),
            'money' => input("money", ''),
            'remark' => input("memo", ''),
        ];

        if ($data['return_time'] != 0 || $data['return_time'] != '') {
            $data['return_time'] = strtotime($data['return_time']);
        }
        
        //验证内容
        try {
            validate(ReceivablesValidate::class)->check([
                'id' => input('id', ''),
                'check_user' => input('check_user', '')
            ] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;

            $m = ReceivablesModel::create($data);

            $msg['id'] = $m->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $m = ReceivablesModel::where('id', input('id', ''))->find();
            $data["update_time"] = time();
            $m->save($data);
            $msg['msg'] = "修改成功";
        }

        // 添加审批人 记得写入message 表
        if (count($m->pass) < 1 && input('check_user', '') != '') {
            $m->pass()->where([])->delete();
            $m->pass()->save([
                'check_user_id' => input('check_user', ''),
                'status' => 0,
                'check_time' => 0,
                'check_remark' => '',
                'order' => 1,
            ]);
        }

        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $modo = ReceivablesModel::where(["id" => $id])->find();
        $modo->customer;
        $modo->contract;
        foreach ($modo->pass as $p) {
            $p->checkuser;
        }
        $msg['modo'] = $modo;
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));

        //检测关联

        $models = ReceivablesModel::where([['id', 'in', $ids]])->select();
        foreach ($models as $m) {
            $m->pass()->where([])->delete();
        }

        ReceivablesModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
