<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\BusinessModel;
use app\admin\validate\crm\BusinessValidate;
use think\exception\ValidateException;

/**
 * 商机控制器
 */
class Business
{
    public $rule = [
        'index' => 'crm.business.view',
        'form' => 'crm.business.add',
        'get' => 'crm.business.edit',
        'del' => 'crm.business.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $customer = input('customer', '');

        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }

        if ($customer != '') {
            array_push($where, ["customer_id", '=', $customer]);
        }

        $obj = BusinessModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->customer;
            $o->status;
            foreach ($o->product as $pr) {
                $pr->category;
            }
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'name' => input('name', ''),
            'customer_id' => input('customer', ''),
            'status_id' => input("status", ''),
            'money' => input("money", ''),
            'total_price' => input("total_price", 0),
            'deal_date' => input("deal_date", ''),
            'discount_rate' => input("discount_rate", 0),
            'remark' => input("memo", ''),
        ];
        if ($data['deal_date'] != 0 || $data['deal_date'] != '') {
            $data['deal_date'] = strtotime($data['deal_date']);
        }
        //验证内容
        try {
            validate(BusinessValidate::class)->check(['id' => input('id', ''),] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["next_time"] = 0;
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;
            $data["ro_user_id"] = '';
            $data["rw_user_id"] = '';

            $m = BusinessModel::create($data);
            $msg['id'] = $m->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $m = BusinessModel::where('id', input('id', ''))->find();
            $data["update_time"] = time();
            $m->save($data);
            $msg['msg'] = "修改成功";
        }

        $product = json_decode(input('product', ''));
        $m->product()->detach();
        if (count($product) > 0) {
            foreach ($product as $p) {
                $m->product()->attach($p->id, [
                    'price' => $p->price,
                    'sales_price' => $p->sales_price,
                    'num' => $p->num,
                    'discount' => $p->discount,
                    'subtotal' => $p->subtotal,
                    'unit' => $p->unit,
                ]);
            }
        }
        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $modo = BusinessModel::where(["id" => $id])->find();
        $modo->customer;
        foreach ($modo->product as $p) {
            $p->category;
        }
        $msg['modo'] = $modo;
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));

        //检测关联

        $models = BusinessModel::where([['id', 'in', $ids]])->select();
        foreach ($models as $m) {
            $m->product()->detach();
        }

        BusinessModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
