<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\CustomerOriginModel;

class CustomerOrigin
{
    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $depart = input('depart', '');
        $where = [];
        if ($key != '') {
            array_push($where, ["username", 'like', '%' . $key . '%']);
        }

        $obj = CustomerOriginModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();

        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }
}
