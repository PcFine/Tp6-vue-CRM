<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\ProductModel;
use app\admin\model\system\FileModel;
use app\admin\validate\crm\ProductValidate;
use think\exception\ValidateException;
use think\facade\Db;

class Product
{
    public $rule = [
        'index' => 'crm.product.view',
        'form' => 'crm.product.add',
        'get' => 'crm.product.edit',
        'del' => 'crm.product.del',
        'file_del' => 'crm.product.edit',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $p_code = input('p_code', '');

        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }

        if ($p_code != '') {
            array_push($where, ["p_code", '=', $p_code]);
        }

        //当前登陆人  负责,拥有读权限的客户 创建人员
        $user = user();
        foreach ($user->group as $g) {
            if ($g->id != 1 && $g->id != 2) {
                array_push($where, ["owner_user_id", '=', $user->id]);
            }
        }

        $obj = ProductModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->files;
            $o->category;
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'name' => input('name', ''),
            'code' => input('code', ''),
            'unit' => input("unit", ''),
            'price' => input("price", ''),
            'status' => input("status", ''),
            'category_id' => input("category", ''),
            'description' => input("memo", ''),
        ];

        //验证内容
        try {
            validate(ProductValidate::class)->check(['id' => input('id', ''),] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;

            $m = ProductModel::create($data);
            $msg['id'] = $m->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $m = ProductModel::where('id', input('id', 0))->find();
            $data["update_time"] = time();
            $m->save($data);
            // $m = ProductModel::update($data, ["id" => input('id', 0)]);
            $msg['msg'] = "修改成功";
        }
        $files = input('files', '');
        $m->files()->detach();
        if ($files != '') {
            $fs = explode(',', $files);
            if (count($fs) > 0) {
                $m->files()->saveAll($fs);
            }
        }
        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');

        $modo =  ProductModel::where(["id" => $id])->find();
        $modo->files;
        $msg['modo'] = $modo;
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));
        //检测关联
        $pro_list = ProductModel::where([['id', 'in', $ids]])->select();
        foreach ($pro_list as $p) {
            $p->files()->detach();
        }
        ProductModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }



    public function file_del()
    {
        $msg = res_msg('');
        // 删除中间表图片数据
        $product = ProductModel::where('id', input('id', ''))->find();
        $ids = explode(',', input('file', ''));
        $product->files()->detach($ids);
        $msg['msg'] = "删除成功";

        return json($msg);
    }
}
