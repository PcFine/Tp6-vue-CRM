<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\ContactsModel;
use app\admin\validate\crm\ContactsValidate;
use think\exception\ValidateException;
use think\Request;

//联系人模块
class Contacts
{
    public $rule = [
        'index' => 'crm.contacts.view',
        'form' => 'crm.contacts.add',
        'get' => 'crm.contacts.edit',
        'del' => 'crm.contacts.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $p_code = input('p_code', '');
        $customer = input('customer', '');

        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }

        if ($p_code != '') {
            array_push($where, ["p_code", '=', $p_code]);
        }

        if ($customer != '') {
            array_push($where, ["customer_id", '=', $customer]);
        }

        $obj = ContactsModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->customer;
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'name' => input('name', ''),
            'customer_id' => input('customer', ''),
            'sex' => input("sex", ''),
            'decision' => input("decision", ''),
            'mobile' => input("mobile", ''),
            'telephone' => input("telephone", ''),
            'email' => input("email", ''),
            'address' => input("address", ''),
            'post' => input("post", ''),
            'next_time' => input("next_time", 0),
            'remark' => input("memo", ''),
        ];
        if ($data['next_time'] != 0 || $data['next_time'] != '') {
            $data['next_time'] = strtotime($data['next_time']);
        }
        //验证内容
        try {
            validate(ContactsValidate::class)->check(['id' => input('id', ''),] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;
            $data["ro_user_id"] = '';
            $data["rw_user_id"] = '';

            $rule = ContactsModel::create($data);
            $msg['id'] = $rule->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $data["update_time"] = time();
            $rule = ContactsModel::update($data, ["id" => input('id', 0)]);
            $msg['msg'] = "修改成功";
        }

        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $modo = ContactsModel::where(["id" => $id])->find();
        $modo->customer;
        $msg['modo'] = $modo;
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));
        //检测关联

        ContactsModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
