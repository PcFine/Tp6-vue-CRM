<?php

declare(strict_types=1);

namespace app\admin\controller\crm;

use app\admin\model\crm\CustomerModel;
use app\admin\validate\crm\CustomerValidate;
use think\exception\ValidateException;

class Customer
{
    public $rule = [
        'index' => 'crm.customer.view',
        'form' => 'crm.customer.add',
        'get' => 'crm.customer.edit',
        'del' => 'crm.customer.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $p_code = input('p_code', '');

        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }

        if ($p_code != '') {
            array_push($where, ["p_code", '=', $p_code]);
        }

        //当前登陆人  负责,拥有读权限的客户 创建人员
        $user = user();
        foreach ($user->group as $g) {
            if ($g->id != 1 && $g->id != 2) {
                array_push($where, ["owner_user_id", '=', $user->id]);
            }
        }

        $obj = CustomerModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        foreach ($obj as $o) {
            $o->levels;
            $o->industrys;
            $o->sources;
            $o->provinces;
            $o->citys;
            $o->districts;
        }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $user = user();
        if (input('action', '') == '') {
            $msg = res_msg('操作类型无效', '404');
            return json($msg);
        }

        $data = [
            'name' => input('name', ''),
            'level' => input('level', 0),
            'industry' => input("industry", 0),
            'source' => input("source", 0),
            'website' => input("website", ''),
            'mobile' => input("mobile", ''),
            'telephone' => input("telephone", ''),
            'email' => input("email", ''),
            'address' => input("address", ''),
            'province' => input("province", ''),
            'city' => input("city", ''),
            'district' => input("district", ''),
            'lnglat' => input("lnglat", ''),
            'next_time' => input("next_time", 0),
            'remark' => input("memo", ''),
        ];
        if ($data['next_time'] != 0 || $data['next_time'] != '') {
            $data['next_time'] = strtotime($data['next_time']);
        }
        //验证内容
        try {
            validate(CustomerValidate::class)->check(['id' => input('id', ''),] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if (input('action') == "add") {
            $data["deal_time"] = time();
            $data["create_time"] = time();
            $data["update_time"] = time();
            $data["is_lock"] = 0;
            $data["deal_status"] = 0;
            $data["create_user_id"] = $user->id;
            $data["owner_user_id"] = $user->id;
            $data["ro_user_id"] = '';
            $data["rw_user_id"] = '';
            $data["follow"] = '';

            $m = CustomerModel::create($data);
            $msg['id'] = $m->id;
            $msg['msg'] = "新增成功";
        }

        if (input('action') == 'edit') {
            $data["update_time"] = time();
            $m = CustomerModel::update($data, ["id" => input('id', 0)]);
            $msg['msg'] = "修改成功";
        }

        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $msg['modo'] = CustomerModel::where(["id" => $id])->find();
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));
        //检测关联

        CustomerModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
