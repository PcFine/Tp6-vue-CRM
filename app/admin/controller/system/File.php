<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\FileModel;
use think\facade\Filesystem;

class File
{
    public function upload()
    {
        $msg = res_msg('');
        $modo = input('modo');
        $file = request()->file('file');
        if ($file) {
            $user = user();
            // halt($file->filename);
            $filename = $file->getFilename();
            $ext = '.' . $file->getExtension();
            $size = $file->getSize();
            $savepath =  Filesystem::disk($modo)->putFile('', $file, 'md5');
            $savename = basename($savepath);
            $savepath = '/assets/' . $modo . '/' . $savepath;

            $save = FileModel::create([
                'modo' => $modo,
                'name' => $filename,
                'save_name' => $savename,
                'file_path' => $savepath,
                'size' => (int) ($size / 1024),
                'types' => $ext,
                'create_user_id' =>  $user->id,
                'create_time' => time(),
                'file_path_thumb' => '',
            ]);

            $msg['id'] = $save->id;
            $msg['filename'] = $filename;
            $msg['url'] =  $savepath;
        } else {
            $msg = res_msg('未找寻到文件信息', '404');
        }
        return json($msg);
    }

    public function delete()
    {
        $msg = res_msg('');
        $ids = input('ids');
        $id = explode(',', $ids);
        FileModel::destroy($id);
        $msg['msg'] = "删除成功";
        return json($msg);
    }
}
