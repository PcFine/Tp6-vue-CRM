<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\AdminUserModel;
use app\admin\validate\system\AdminUserValidate;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * 操作员及员工 控制器
 */
class AdminUser
{
  public $rule = [
    'index' => 'system.deparUser.view',
    'form' => 'system.deparUser.add',
    'get' => 'system.deparUser.edit',
    'del' => 'system.deparUser.del',
    'get_rule' => 'system.deparUser.rule',
    'set_rule' => 'system.deparUser.rule',
  ];

  public function index()
  {
    $msg = res_msg('');
    $p = input('page', 0);
    $s = input('size', 10);
    $sum = 1;

    $key = input('key', '');
    $depart = input('depart', '');
    $myself = input('myself', '1');

    $where = [];
    if ($key != '') {
      array_push($where, ["realname", 'like', '%' . $key . '%']);
    }

    if ($depart != '') {
      array_push($where, ["depart_id", '=',  $depart]);
    }

    //过滤掉本身 
    if ($myself == '1') {
      halt($myself);
      $user = user();
      if ($user) {
        array_push($where, ["id", '<>',  $user->id]);
      }
      //不能获取超级admin用户
      array_push($where, ["id", '<>',  1]);
    }

    $obj = AdminUserModel::where($where);
    $sum = $obj->select()->count();
    if ($p != 0) {
      $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
    }
    $obj = $obj->select();
    // foreach ($obj as &$o) {
    //   $o->parent;
    // }
    $msg['list'] = $obj->toArray();
    $msg['sum'] = $sum == 0 ? 1 : $sum;
    $msg['p'] = (int) $p;
    return json($msg);
  }

  public function form()
  {
    $msg = res_msg('');
    $action = input('action', '');
    $id = input('id', '');
    $data = [
      'username' => input('username', ''),
      'num' => input('code', ''),
      'realname' => input('name', ''),
      'sex' => input('sex', ''),
      'parent_id' => input('p_user', ''),
      'post' => input('post', ''),
      'depart_id' => input('depart', ''),
      'type' => input('type', ''),
      'mobile' => input('mobile', ''),
      'email' => input('email', ''),
      'address' => input('address', ''),
      'status' => input('status', 1),
      'content' => input('memo', ''),
      'update_time' => time(),
    ];

    //验证内容
    try {
      validate(AdminUserValidate::class)->check(['id' => input('id', ''),] + $data);
    } catch (ValidateException $err) {
      return json(res_msg($err->getError(), '404'));
    };

    //角 色
    $groups = [];
    if (input('groups_str', '') != '') {
      $groups = explode(',', input('groups_str', ''));
    }

    if ($action == 'add') {
      $admin = new AdminUserModel();

      //验证密码
      $pwd = input('password', '');
      if ($pwd == '') {
        return json(res_msg('密码不能空', '404'));
      }

      if (strlen($pwd) < 6) {
        return json(res_msg('密码长度不能小于6', '404'));
      }

      $data['password'] = create_pwd($pwd);
      $data['img'] = '';
      $data['mini_img'] = '';
      $data['auth_key'] = '';
      $data['auth_key_time'] = 0;
      $data['last_login_time'] = 0;
      $data['last_login_ip'] = '';
      $data['login_count'] = 0;
      $data['create_time'] = time();

      $admin->save($data);
      $msg['msg'] = '新增成功';
    }

    if ($action == 'edit') {
      $admin = AdminUserModel::where('id', $id)->find();
      AdminUserModel::update($data, ['id' => $id]);
      $msg['msg'] = '修改成功';

      $admin->group()->detach();
    }

    //添加角色
    if ($admin) {
      if (count($groups) > 0) {
        $admin->group()->saveAll($groups);
      }
    }

    return json($msg);
  }

  public function get()
  {
    $msg = res_msg('');
    $modo = AdminUserModel::where('id', (input('id') ?? ''))->find();
    $modo->group->toArray();
    // foreach ($group as $n) {
    //   $n->rule;
    // }
    // $modo->rule;
    $msg['modo'] =  $modo;
    return json($msg);
  }

  public function del()
  {
    $msg = res_msg('');
    $ids = explode(',', input('id', ''));

    //检测关联
    $users = AdminUserModel::where([['id', 'in', $ids]])->select();
    foreach ($users as $u) {
      $u->group()->detach();
      $u->delete();
    };
    // AdminUserModel::destroy($ids);
    $msg['msg'] = '删除成功';
    return json($msg);
  }

  //获取用户权限信息
  public function get_rule()
  {
    $msg = res_msg('');
    $obj = AdminUserModel::where('id', input('id', ''))->find();
    $obj->rule;
    foreach ($obj->group as $g) {
      $g->rule;
    }
    $msg['rule'] = $obj->rule;
    $msg['group'] = $obj->group;
    return json($msg);
  }

  //设置j用户权限信息
  public function set_rule()
  {
    $msg = res_msg('');

    $arr = json_decode(input('select', '[]'));

    $obj = AdminUserModel::where('id', input('id', ''))->find();
    if ($obj) {
      $obj->rule()->detach();
      foreach ($arr as $a) {
        if ($a->check) {
          $obj->rule()->save($a->id);
        }
      }
      //更新缓存中的权限
      $user = AdminUserModel::where('id', input('id', ''))->find();
      $old = cache($user->auth_key);
      if ($old) {
        $old['rule'] = rules($user, false);
        cache($user->auth_key, $old, config('app.jwt_exp', 1800));
      }
      $msg['msg'] = "设置成功";
    }
    return json($msg);
  }
}
