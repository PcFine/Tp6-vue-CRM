<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\AdminUserModel;
use app\admin\model\system\AuthRuleModel;
use app\admin\validate\system\RuleValidate;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * 权限控制器
 */
class Rule
{

  public $rule = [
    'index' => 'system.rule.view',
    'form' => 'system.rule.add',
    'get' => 'system.rule.edit',
    'del' => 'system.rule.del',
  ];

  public function index()
  {
    $msg = res_msg('');
    $p = input('page', 0);
    $s = input('size', 10);
    $sum = 1;

    $key = input('key', '');
    $where = [];
    if ($key != '') {
      array_push($where, ["name", 'like', '%' . $key . '%']);
    }
    $obj = AuthRuleModel::where($where)->order('order');
    $sum = $obj->select()->count();
    if ($p != 0) {
      $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
    }
    $obj = $obj->select();
    foreach ($obj as &$o) {
      $o->parent;
    }
    $msg['list'] = $obj->toArray();
    $msg['sum'] = $sum == 0 ? 1 : $sum;
    $msg['p'] = (int) $p;
    return json($msg);
  }

  public function get()
  {
    $msg = res_msg('');
    $id = input('id', '');
    $msg['modo'] = AuthRuleModel::where(["id" => $id])->find();
    return json($msg);
  }

  public function form()
  {
    $msg = res_msg('');

    if (input('action', '') == '') {
      $msg = res_msg('操作类型无效', '404');
      return json($msg);
    }

    $data = [
      'parent_code' => '',
      'parent_id' => input('parent', 0),
      'code' => input("code", ''),
      'name' => input("name", ''),
      'url' => input("url", ''),
      'icon' => input("icon", ''),
      'status' => input("status", 1),
    ];
    $order = '';
    $type = '';
    $index = AuthRuleModel::where(['parent_id' =>  $data['parent_id']])->select()->count('id') + 1;
    if ($data['parent_id'] != 0) {
      $p = AuthRuleModel::where(['id' => $data['parent_id']])->find();
      if ($p) {
        $order = $p->order . '_' . $data['code'];
        $data['parent_code'] = $p->code;
        if ($p->type == 'modo') {
          $type = 'nav';
        }
        if ($p->type == 'nav') {
          $type = 'action';
        }
      }
    } else {
      $type = 'modo';
    }
    $data['order'] = $order;
    $data['type'] = $type;

    //验证内容
    try {
      validate(RuleValidate::class)->check(['id' => input('id', ''),] + $data);
    } catch (ValidateException $err) {
      return json(res_msg($err->getError(), '404'));
    };

    if (input('action') == "add") {
      $type = '';
      $data["add_time"] = date("Y-m-d H:i:s", time());
      $data['index'] = $index;

      $rule = AuthRuleModel::create($data);
      $msg['id'] = $rule->id;
      $msg['msg'] = "新增成功";
    }

    if (input('action') == 'edit') {
      $rule = AuthRuleModel::update($data, ["id" => input('id', 0)]);
      $msg['msg'] = "修改成功";
    }

    return json($msg);
  }

  public function del()
  {
    $msg = res_msg('');
    $ids = explode(',', input('id', ''));
    //检测关联
    AuthRuleModel::destroy($ids);
    $msg['msg'] = '删除成功';
    return json($msg);
  }

  public function rule_type()
  {
    $msg = res_msg('');

    //当包含开发者时,才开放权限信息行 供提供权限赋予
    $user = user();
    $isNotRule = 'rule';
    if ($user) {
      $group =  $user->group;
      foreach ($group as $g) {
        if ($g->id == 1) {
          $isNotRule = '';
        }
      }
    }

    $cols = Db::query("SELECT `code` `key`, `code`,`name` FROM `auth_rule` 
                      where type='action'
                      group by `code`,`name`
                      order by `name` desc");

    $modo = Db::query("SELECT `code` `key`,`name` `modo`, `code`,`name` FROM `auth_rule` 
                    where type='modo'
                    order by `name`");

    //修改下 检索是否有对应权限的列
    // $nav_action = "ifnull((select MAX(id) from `auth_rule` AA where `AA`.parent_id=`A`.`id` and `AA`.`code`='add'),'') `add`";
    $nav_action = "";
    foreach ($cols as $c) {
      $nav_action = $nav_action . ",ifnull((select MAX(id) from `auth_rule` AA where `AA`.parent_id=`A`.`id` and `AA`.`code`='" . $c['key'] . "'),'') `" . $c['key'] . "`";
    }
    $navs = Db::query("SELECT id,`code` `key`,`name` `nav`, `code`,`name`,`parent_code` parent " . $nav_action . " 
                      FROM `auth_rule` A
                      where type='nav' and code != '" . $isNotRule . "'
                      order by `name` desc;");


    $msg['modo'] = $modo;
    $msg['cols'] = $cols;
    $msg['navs'] = $navs;
    return json($msg);
  }
  
}
