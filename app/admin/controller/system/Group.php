<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\AdminUserModel;
use app\admin\model\system\AuthGroupModel;
use app\admin\validate\system\GroupValidate;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * 角色控制器
 */
class Group
{

  public $rule = [
    'index' => 'system.group.view',
    'form' => 'system.group.add',
    'get' => 'system.group.edit',
    'del' => 'system.group.del',
    'get_rule' => 'system.group.rule',
    'set_rule' => 'system.group.rule',
  ];

  public function index()
  {
    $msg = res_msg('');
    $p = input('page', 0);
    $s = input('size', 10);
    $sum = 1;

    $key = input('key', '');
    $where = [];
    if ($key != '') {
      array_push($where, ["name", 'like', '%' . $key . '%']);
    }

    //禁止非开发者 角色用户获取到开发者的信息
    $user = user();
    if ($user) {
      $flag = false;
      foreach ($user->group as $g) {
        if ($g->id == 1) {
          $flag = true;
          break;
        }
      }
      if (!$flag) {
        array_push($where, ["id", '<>', '1']);
      }
    }

    //禁止非超级管理员 角色用户获取到超级管理员的信息
    if ($user) {
      $flag = false;
      foreach ($user->group as $g) {
        if ($g->id == 2 || $g->id == 1) {
          $flag = true;
          break;
        }
      }
      if (!$flag) {
        array_push($where, ["id", '<>', '2']);
      }
    }


    $obj = AuthGroupModel::where($where);
    $sum = $obj->select()->count();
    if ($p != 0) {
      $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
    }
    $obj = $obj->select();



    $msg['list'] = $obj->toArray();
    $msg['sum'] = $sum == 0 ? 1 : $sum;
    $msg['p'] = (int) $p;
    return json($msg);
  }

  public function get()
  {
    $msg = res_msg('');
    $id = input('id', '');
    $msg["modo"] = AuthGroupModel::where(['id' => $id])->find();
    return json($msg);
  }

  public function form()
  {
    $msg = res_msg('');
    $action = input('action', '');
    $data = [
      'name' => input('name', ''),
      'description' => input('memo', ''),
      'status' => input('status', 1),
    ];
    //验证内容
    try {
      validate(GroupValidate::class)->check(['id' => input('id', ''),] + $data);
    } catch (ValidateException $err) {
      return json(res_msg($err->getError(), '404'));
    };

    if ($action == 'add') {
      $group = AuthGroupModel::create($data);
      $msg['id'] = $group->id;
      $msg['msg'] = "新增成功";
    }

    if ($action == 'edit') {
      $group = AuthGroupModel::update($data, ["id" => input('id', 0)]);
      $msg['msg'] = "修改成功";
    }
    return json($msg);
  }

  public function del()
  {
    $msg = res_msg('');
    $ids = explode(',', input('id', ''));
    //检测关联

    AuthGroupModel::destroy($ids);
    $msg['msg'] = '删除成功';
    return json($msg);
  }

  //获取角色权限信息
  public function get_rule()
  {
    $msg = res_msg('');
    $obj = AuthGroupModel::where('id', input('id', ''))->find();
    $msg['rule'] = $obj->rule;
    return json($msg);
  }

  //设置角色权限信息
  public function set_rule()
  {
    $msg = res_msg('');

    $arr = json_decode(input('select', '[]'));

    $obj = AuthGroupModel::where('id', input('id', ''))->find();
    if ($obj) {
      $obj->rule()->detach();
      foreach ($arr as $a) {
        if ($a->check) {
          $obj->rule()->save($a->id);
        }
      }
      //设置好后 所有带有这个角色的所有用户 读取auth_key的值 进行 更新缓存
      $list =  Db::query("select A.* from admin_user A 
                left join auth_user_group B on A.id = B.user_id
                where B.group_id='" . $obj->id . "'");

      foreach ($list as $l) {
        $user = AdminUserModel::where('id', $l['id'])->find();
        if ($user) {
          $old =  cache($l['auth_key']);
          if ($old) {
            $old['rule'] = rules($user, false);
            // halt($old);
            cache($l['auth_key'], $old, config('app.jwt_exp', 1800));
          }
        }
      }
      $msg['msg'] = "设置成功";
    }
    return json($msg);
  }
}
