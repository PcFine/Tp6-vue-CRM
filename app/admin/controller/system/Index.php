<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\AdminUserModel;

/**
 * 基本后台登陆 验证等操作控制器
 */
class Index
{
    public function index()
    {
        echo phpinfo();
        exit();
    }

    public function login()
    {
        $msg = res_msg('');

        //若$action 等于 force 为重新强制登录
        $action = input('action', '');
        //验证用户 密码
        $U  = AdminUserModel::where('username', input('username', ''))->find();
        if (!$U) {
            $msg = res_msg('账号不存在', '404');
            return json($msg);
        }

        if (!$U->check_pwd(input('code', ''))) {
            $msg = res_msg('密码错误', '404');
            return json($msg);
        }
        
        if ($U->status != 1) {
            $msg = res_msg('账号已禁用或者未激活', '404');
            return json($msg);
        }

        //生成auth_key值
        $guid = str_replace('-', '', strtolower(guid()));
        // $msg['123'] = cache('eec18c74c751a43530b6070a371102a7');
        //检测是否已登录
        if (($U->auth_key ?? '') != '') {
            //检测 是否失效
            $cache = cache($U->auth_key);
            if ($cache) {
                //询问是否重新登录
                if ($action == '') {
                    $msg = res_msg('当前用户已登录,是否重新登录(此操作15秒内有效)?', '901');
                    cache($guid, 'exist', 15);
                    $msg['auth_key'] = $guid;
                    return json($msg);
                }
                if ($action == 'force') {
                    //将原先guid等信息 扔入黑名单
                    if (input('auth_key', '') != '') {
                        //是否存在auth_key 缓存
                        if (cache(input('auth_key', ''))) {
                            //获取旧的guid auth_key 清空旧的缓存
                            // in_auth_key_black(($U->auth_key));
                            cache($U->auth_key, null);
                        } else {
                            return json(res_msg('提交信息错误,登录操作无效', '404'));
                        }
                    } else {
                        return json(res_msg('提交信息错误,登录操作无效', '404'));
                    }
                }
            }
        }

        //获取权限信息
        $msg['rule'] = rules($U, false);

        if (count($msg['rule']) == 0) {
            $msg = res_msg('暂无权限, 无法登录', '404');
            return json($msg);
        }

        #创建用户信息  原jwt
        $user = [
            "user_id" => $U->id,
            "user_name" => $U->username,
            "type" => $U->type,
            "auth_key" => $guid,
            "rule" => $msg['rule'],
        ];

        //写入缓存jwt 写入headerjwt 更新用户 auth_key的guid
        cache($guid, $user, config('app.jwt_exp', 1800));
        // header("Auth:" . $jwt);
        AdminUserModel::update(
            [
                'auth_key' => $guid,
                'last_login_ip' => get_client_ip(),
                'last_login_time' => time(),
                'login_count' => $U->login_count + 1,
            ],
            ["id" => $U->id]
        );

        $msg['Auth'] = $guid;
        $msg['UserName'] = $U->username;
        $msg["msg"] = "登录成功";
        return json($msg);
    }

    //获取权限信息
    public function get_rule()
    {
        $msg = res_msg('');
        $user = user();
        $msg['rule'] = cache($user->auth_key)['rule'];
        return json($msg);
    }

    public function login_out()
    {
        $msg = res_msg('');
        //jwt_black jwt黑名单 黑名单里面的jwt将不能再使用
        $auth_key = request()->header('Auth', '');
        cache($auth_key, null);
        // if ($cache) {
        //     // in_jwt_black($cache['jwt']);
        //     cache($auth_key, null);
        // }
        $msg['msg'] = '退出成功';
        return json($msg);
    }
}
