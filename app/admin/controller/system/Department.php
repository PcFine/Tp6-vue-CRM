<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\model\system\DepartmentModel;
use app\admin\validate\system\DepartmentValidate;
use think\exception\ValidateException;


/**
 * 部门 控制器
 */
class Department
{
    public $rule = [
        'index' => 'system.deparUser.view',
        'form' => 'system.deparUser.add',
        'get' => 'system.deparUser.edit',
        'del' => 'system.deparUser.del',
    ];

    public function index()
    {
        $msg = res_msg('');
        $p = input('page', 0);
        $s = input('size', 10);
        $sum = 1;

        $key = input('key', '');
        $where = [];
        if ($key != '') {
            array_push($where, ["name", 'like', '%' . $key . '%']);
        }
        $obj = DepartmentModel::where($where);
        $sum = $obj->select()->count();
        if ($p != 0) {
            $obj = $obj->limit((($p - 1) * $s) == 0 ? 0 : (($p - 1) * $s), (int) $s);
        }
        $obj = $obj->select();
        // foreach ($obj as &$o) {
        //   $o->parent;
        // }
        $msg['list'] = $obj->toArray();
        $msg['sum'] = $sum == 0 ? 1 : $sum;
        $msg['p'] = (int) $p;
        return json($msg);
    }

    public function form()
    {
        $msg = res_msg('');
        $action = input('action', '');
        $data = [
            'name' => input('name', ''),
            'remarks' => input('memo', ''),
            'parent_id' => input('parent', ''),
        ];
        //验证内容
        try {
            validate(DepartmentValidate::class)->check(['id' => input('id', ''),] + $data);
        } catch (ValidateException $err) {
            return json(res_msg($err->getError(), '404'));
        };

        if ($action == 'add') {
            $depart = DepartmentModel::create($data);
            $msg['id'] = $depart->id;
            $msg['msg'] = "新增成功";
        }

        if ($action == 'edit') {
            $depart = DepartmentModel::update($data, ["id" => input('id', 0)]);
            $msg['msg'] = "修改成功";
        }
        return json($msg);
    }

    public function get()
    {
        $msg = res_msg('');
        $id = input('id', '');
        $msg["modo"] = DepartmentModel::where(['id' => $id])->find();
        return json($msg);
    }

    public function del()
    {
        $msg = res_msg('');
        $ids = explode(',', input('id', ''));
        //检测关联

        DepartmentModel::destroy($ids);
        $msg['msg'] = '删除成功';
        return json($msg);
    }
}
