<?php

declare(strict_types=1);

namespace app\admin\validate\crm;

use think\Validate;

class BusinessValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name' => 'require|max:32',
        'customer_id' => 'require',
        'status_id' => 'require',
        'deal_date' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require' => '商机名称必须输入',
        'name.max' => '商机名称过长(<32)',
        'customer_id.require' => '请选择客户',
        'status_id' => '请选择商机阶段',
        'deal_date.require' => '请输入预计成交日期'
    ];
}
