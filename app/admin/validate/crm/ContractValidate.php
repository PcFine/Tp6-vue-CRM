<?php

declare(strict_types=1);

namespace app\admin\validate\crm;

use app\admin\model\crm\ContractModel;
use think\Validate;

class ContractValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'code' => ['require', 'exist'],
        'name' => ['require', 'exist'],
        'customer_id' => ['require'],
        'money' => ['require', 'min:0'],
        'order_date' => ['require'],
        'check_user' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'code.require' => '合同编号必须输入',
        'name.require' => '合同名称必须输入',
        'money.require' => '请输入合同金额',
        'money.min' => '合同金额必须大于0',
        'customer_id.require' => '客户必须选择',
        'order_date.require' => '下单时间必须选择',
        'check_user.require' => '请选择审核人'
    ];

    public function exist($value, $rule, $data = [], $field = '')
    {
        if ($field == 'code') {
            $i =  ContractModel::where('id', '<>', $data['id'])->where('code', $value)->select()->count();
            if ($i > 0) {
                return '编号已存在';
            }
        }
        if ($field == 'name') {
            $i =  ContractModel::where('id', '<>', $data['id'])->where('name', $value)->select()->count();
            if ($i > 0) {
                return '名称已存在';
            }
        }
        return true;
    }
}
