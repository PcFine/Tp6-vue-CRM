<?php
declare (strict_types = 1);

namespace app\admin\validate\crm;

use app\admin\model\crm\ProductModel;
use think\Validate;

class ProductValidate extends Validate
{
   /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name' => ['require', 'exist'],
        'code' => ['require', 'exist'],
        'status' => ['require'],
        'category_id' => ['require'],
        'price' => ['require'],
        'unit' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require' => '名称必须输入',
        'code.require' => '编号必须输入',
        'status.require' => '请选择是否上架',
        'unit.require' => '单位必须输入',
        'price.require' => '标准单价必须输入',
        'category_id.require' => '产品类别必须输入'
    ];

    public function exist($value, $rule, $data = [], $field = '')
    {
        if ($field == 'name') {
            $i =  ProductModel::where('id', '<>', $data['id'])->where('name', $value)->select()->count();
            if ($i > 0) {
                return '名称已存在';
            }
        }

        if ($field == 'code') {
            $i =  ProductModel::where('id', '<>', $data['id'])->where('code', $value)->select()->count();
            if ($i > 0) {
                return '编号已存在';
            }
        }
        return true;
    }
}
