<?php

declare(strict_types=1);

namespace app\admin\validate\crm;

use app\admin\model\crm\ReceivablesModel;
use think\Validate;

class ReceivablesValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'code' => ['require', 'exist'],
        'customer_id' => ['require'],
        'contract_id' => ['require'],
        'money' => ['require', 'min:0'],
        'check_user' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'code.require' => '汇款编号必须输入',
        'money.require' => '请输入合同金额',
        'money.min' => '合同金额必须大于0',
        'customer_id.require' => '客户必须选择',
        'contract_id.require' => '合同必须选择',
        'check_user.require' => '请选择审核人'
    ];

    public function exist($value, $rule, $data = [], $field = '')
    {
        if ($field == 'code') {
            $i =  ReceivablesModel::where('id', '<>', $data['id'])->where('code', $value)->select()->count();
            if ($i > 0) {
                return '编号已存在';
            }
        }
        return true;
    }
}
