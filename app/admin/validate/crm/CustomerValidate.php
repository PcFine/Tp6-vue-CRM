<?php

declare(strict_types=1);

namespace app\admin\validate\crm;

use app\admin\model\crm\CustomerModel;
use think\Validate;

class CustomerValidate extends Validate
{

    protected $rule = [
        'name' => 'require|max:32|check_exist'
    ];


    protected $message = [
        'name.require' => '客户名称必须输入',
        'name.max' => '客户名称长度过长(<32)'
    ];


    public function check_exist($value, $rule, $data = [], $field = '')
    {
        if ($field == 'name') {
            $i =  CustomerModel::where('id', '<>', $data['id'])->where('name', $value)->select()->count();
            if ($i > 0) {
                return '名称已存在';
            }
        }
        return true;
    }
}
