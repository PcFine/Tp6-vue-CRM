<?php

declare(strict_types=1);

namespace app\admin\validate\crm;

use app\admin\model\crm\ContactsModel;
use think\Validate;

class ContactsValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name' => 'require|max:32',
        'customer_id' => 'require'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require' => '联系人名称必须输入',
        'name.max' => '联系人名称过长(<32)',
        'customer_id.require' => '请选择客户'
    ];
}
