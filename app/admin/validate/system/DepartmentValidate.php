<?php

declare(strict_types=1);

namespace app\admin\validate\system;

use app\admin\model\system\AdminUserModel;
use app\admin\model\system\DepartmentModel;
use think\Validate;

class DepartmentValidate extends Validate
{
  protected $rule = [
    'name'  =>  'require|max:32|checkExist',
    'parent_id' => 'parent'
    // 'type' => 'require|checkType',
  ];

  protected $message = [
    'name.require' => '名称必须输入',
    'name.max' => '名称内容长度过长(<32)',
    // 'type.require' => '类型必须输入',
  ];


  // 自定义验证规则
  protected function checkExist($value, $rule, $data = [], $field = '')
  {
    if ($field == 'name') {
      $i =  DepartmentModel::where('id', '<>', $data['id'])->where('parent_id', $data['parent_id'])->where('name', $value)->select()->count();
      if ($i > 0) {
        return '名称已存在';
      }
    }
    return true;
  }

  // 自定义验证规则
  protected function parent($value, $rule, $data = [], $field = '')
  {
    if ($field == 'name') {
      $i =  AdminUserModel::where('depar_id', $value)->select()->count();
      if ($i > 0) {
        return '上级部门存在人员信息,操作无效';
      }
    }
    return true;
  }

  
}
