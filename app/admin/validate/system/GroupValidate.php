<?php

namespace app\admin\validate\system;

use app\admin\model\system\AuthGroupModel;
use think\Validate;

class GroupValidate extends Validate
{
  protected $rule = [
    'name'  =>  'require|max:32|checkExist',
    // 'type' => 'require|checkType',
  ];

  protected $message = [
    'name.require' => '名称必须输入',
    'name.max' => '名称内容长度过长(<32)',
    // 'type.require' => '类型必须输入',
  ];


  // 自定义验证规则
  protected function checkExist($value, $rule, $data = [], $field = '')
  {
    if ($field == 'name') {
      $i =  AuthGroupModel::where('id', '<>', $data['id'])->where('name', $value)->select()->count();
      if ($i > 0) {
        return '名称已存在';
      }
    }
    return true;
  }
}
