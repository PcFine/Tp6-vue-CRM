<?php

declare(strict_types=1);

namespace app\admin\validate\system;

use app\admin\model\system\AdminUserModel;
use think\Validate;

class AdminUserValidate extends Validate
{
    protected $rule = [
        'username'  =>  'require|max:32|checkExist',
        'realname'  =>  'require|max:32',
        'num' => 'checkExist',
        'type' => 'require',
        'status' => 'require',
        'sex' => 'require',
        // 'type' => 'require|checkType',
    ];

    protected $message = [
        'username.require' => '用户名必须输入',
        'username.max' => '用户名内容长度过长(<32)',
        'realname.require' => '姓名必须输入',
        'realname.max' => '姓名内容长度过长(<32)',
        'type.require' => '用户类型必须输入',
        'status.require' => '状态必须输入',
        'sex.require' => '性别必须输入',
    ];


    // 自定义验证规则
    protected function checkExist($value, $rule, $data = [], $field = '')
    {
        if ($field == 'username') {
            $i =  AdminUserModel::where('id', '<>', $data['id'])->where('username', $value)->select()->count();
            if ($i > 0) {
                return '用户名已存在';
            }
        }

        if ($field == 'num') {
            if ($value == '') {
                return true;
            }
            $i =  AdminUserModel::where('id', '<>', $data['id'])->where('num', $value)->select()->count();
            if ($i > 0) {
                return '编号已存在';
            }
        }
        return true;
    }
}
