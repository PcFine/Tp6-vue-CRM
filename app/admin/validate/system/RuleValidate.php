<?php

namespace app\admin\validate\system;

use app\admin\model\system\AuthRuleModel;
use think\Validate;

class RuleValidate extends Validate
{
  protected $rule = [
    'code'  =>  'require|max:32|checkExist',
    'name'  =>  'require|max:32|checkExist',
    // 'type' => 'require|checkType',
  ];

  protected $message = [
    'code.require' => '编号必须输入',
    'code.max' => '编号内容长度过长(<32)',
    'name.require' => '名称必须输入',
    'name.max' => '名称内容长度过长(<32)',
    // 'type.require' => '类型必须输入',
  ];

  // 自定义验证规则
  protected function checkType($value, $rule, $data = [])
  {
    if ($value != 'modo' && $data['parent_id'] == '') {
      return '当前类型必须拥有上级';
    }

    if ($value == 'modo' && $data['parent_id'] != '') {
      return '当前类型无需上级';
    }
    return true;
  }

  // 自定义验证规则
  protected function checkExist($value, $rule, $data = [], $field = '')
  {
    if ($field == 'code') {
      $i =  AuthRuleModel::where('id', '<>', $data['id'])->where('code', $value)->where('parent_id',$data['parent_id'])->select()->count();
      if ($i > 0) {
        return '编号已存在';
      }
    }
    if ($field == 'name') {
      $i =  AuthRuleModel::where('id', '<>', $data['id'])->where('name', $value)->where('parent_id',$data['parent_id'])->select()->count();
      if ($i > 0) {
        return '名称已存在';
      }
    }
    return true;
  }
}
