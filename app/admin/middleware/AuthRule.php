<?php

declare(strict_types=1);

namespace app\admin\middleware;

use Exception;

class AuthRule
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //验证权限 主要验证查看 新增 修改 删除等操作
        $user = user();
        try {
            if ($user) {
                //实例化控制器类  读取rule验证数组
                $path = "app\admin\controller\\" . str_replace('.', "\\", $request->controller());
                //助手函数 app实例化 控制器
                $app = app($path);
                $rule = (new $app())->rule;
                $path = explode('.', $rule[$request->action()]);

                //获取缓存中的权限信息
                $rules =cache($user->auth_key)['rule'];
                // halt($path);
                // halt($rules);
                $node = null;
                $flag = false;
                foreach ($path as $p) {
                    if ($node) {
                        foreach ($node['children'] as $r) {
                            if ($r['code'] == $p) {
                                $node = $r;
                                break;
                            }
                        }
                    } else {
                        foreach ($rules as $r) {
                            if ($r['code'] == $p) {
                                $node = $r;
                                break;
                            }
                        }
                    }
                }
                if ($node['type'] == 'action') {
                    $flag = true;
                }
            } else {
                $flag = false;
            }

            if (!$flag) {
                return json(res_msg('无操作权限', '404'));
            }
        } catch (Exception $e) {
            return json(res_msg('无操作权限', '404'));
        }
        // halt($request);
        return $next($request);
    }
}
