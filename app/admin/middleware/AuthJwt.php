<?php

declare(strict_types=1);

namespace app\admin\middleware;

use think\facade\Request;

class AuthJwt
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {

        $flag = false;
        if (!ansy_white($request)) {
            //每次操作更新下 redis中 jwt的值 模范session的过期逻辑
            $auth_key = $request->header('Auth', '');
            $cache = cache($auth_key);
            halt($cache);
            // $flag = !$jwt;
            if ($cache) {
                $jwt = ansy_jwt($cache['jwt']);
                // halt($old);
                if ($jwt) {
                    $jwt = create_jwt([
                        "user_id" => $jwt->user_id,
                        "user_name" => $jwt->user_name,
                        "type" => $jwt->type,
                        "auth_key" => $jwt->auth_key,
                    ]);
                    //将旧的jwt放在黑名单里面
                    in_jwt_black($cache['jwt']);

                    $cache['jwt'] = $jwt;
                    cache($auth_key, $cache, config('app.jwt_exp', 1800));
                    // header('Access-Control-Allow-Headers:Auth');
                    // header('auth:' . $jwt);
                    $flag = false;
                } else {
                    $flag = true;
                }
            } else {
                $flag = true;
            }
        }
        if ($flag) {
            $msg = res_msg('身份令牌错误,请重新登录', '902');
            return json($msg);
        }
        return $next($request);
    }
}
