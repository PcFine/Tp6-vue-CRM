<?php

declare(strict_types=1);

namespace app\admin\middleware;

class AuthLogin
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $flag = false;
        if (!ansy_white($request)) {
            $user = user();
            if ($user) {
                //刷新缓存时间
                $cache = cache($user->auth_key);
                cache($user->auth_key, $cache, config('app.jwt_exp', 1800));
            } else {
                $flag = true;
            }
        }
        if ($flag) {
            $msg = res_msg('未登录,请先登录', '902');
            return json($msg);
        }
        return $next($request);
    }
}
